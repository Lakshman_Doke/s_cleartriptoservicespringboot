package com.digit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/**
 * 
 * @author Lakshman.Doke
 *
 */
@SpringBootApplication
public class SCleartriptoserviceApplication {
    public static void main(String[] args) {
        SpringApplication.run(SCleartriptoserviceApplication.class, args);
    }
}
