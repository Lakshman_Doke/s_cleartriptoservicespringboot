package com.digit.pdfgeneration;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.reactive.function.client.WebClient;

import com.digit.model.Booking;
import com.digit.model.Mail;
import com.digit.model.Upload;
import com.digit.utility.PropertyConfiguration;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParseException;

import reactor.core.publisher.Mono;
/**
 * 
 * @author Lakshman.Doke
 *
 */
public class MailProcess {

	@Autowired
	PropertyConfiguration propertyConfiguration;
	private static final Logger logger = LoggerFactory.getLogger(MailProcess.class);

	public String sendMailWithAttachment(Mail mailPojo)  {
		String transaction = null;
		var gson = new Gson();
		String json = gson.toJson(mailPojo);
		logger.info("sendMailWithAttchement(Mail mailPojo): Email sending with details");
		logger.info("sendMailWithAttchement(Mail mailPojo): Mail: {}", json);

		ResponseEntity<?> responseEntity = WebClient.create(propertyConfiguration.getMailSendUrl()).post()
				.accept(MediaType.APPLICATION_JSON).body(Mono.just(mailPojo), Mail.class).retrieve()
				.toEntity(Mail.class).block();

		if (null != responseEntity && responseEntity.getStatusCode() != HttpStatus.OK) {
			logger.info("sendMailWithAttchement(Mail mailPojo): Email sending failed..");
			return transaction;
		}
		if (responseEntity == null || !responseEntity.hasBody()) {
			return transaction;
		}
		transaction = responseEntity.getBody()== null ? "": responseEntity.getBody().toString();
		return transaction;
	}

	/**
	 * @param booking
	 * @param attachmentLink
	 * @param activityTypeId
	 * @return Mail
	 * 
	 */
	public String getMailObjectAndSendEmail(Booking booking, List<String> attachmentLink, String activityTypeId) {
		var mail = new Mail();
		try {
			String from = null;
			List<String> to = new ArrayList<>();
			List<String> cc = new ArrayList<>();
			List<String> bcc = new ArrayList<>();
			HashMap<String, String> parameters = new HashMap<>();
			HashMap<String, String> subjectParameters = new HashMap<>();
			var policyNumbers = new StringBuilder();
			var destination = "";
			List<String> attachment = new ArrayList<>();

			from = propertyConfiguration.getMailSendUrl();
			String email = null;

			email = booking.getBooking().getPassengers().get(0).getEmail();

			to.add(email);

			parameters.put("name", booking.getBooking().getPassengers().get(0).getFirstName());

			if (booking.getBooking().getPassengers().get(0).getFlights().get(0).getProduct().equalsIgnoreCase("11"))
				parameters.put("link", propertyConfiguration.getTncLinkComp());

			if (booking.getBooking().getPassengers().get(0).getFlights().get(0).getProduct().equalsIgnoreCase("12"))
				parameters.put("link", propertyConfiguration.getTncLinkArc());

			parameters.put("agent", propertyConfiguration.getPartener());

			attachment.addAll(attachmentLink);

			for (var i = 0; i < booking.getBooking().getPassengers().get(0).getFlights().size(); i++) {
				var flight = booking.getBooking().getPassengers().get(0).getFlights().get(i);
				if (flight.getJourneyType().equalsIgnoreCase("Onward")) {
					destination = flight.getDestination();
				}
			}

			for (var i = 0; i < booking.getBooking().getPassengers().size(); i++) {
				if (i != booking.getBooking().getPassengers().size() - 1)
					policyNumbers
							.append(booking.getBooking().getPassengers().get(i).getFlights().get(0).getDIGITPolicyNo())
							.append(" ");
				else if (booking.getBooking().getPassengers().size() > 1)
					policyNumbers.append("and ")
							.append(booking.getBooking().getPassengers().get(i).getFlights().get(0).getDIGITPolicyNo());
				else
					policyNumbers
							.append(booking.getBooking().getPassengers().get(i).getFlights().get(0).getDIGITPolicyNo());
			}
			parameters.put("policyNumber", policyNumbers.toString());

			mail.setFrom(from);
			mail.setTo(to);
			mail.setCc(cc);
			mail.setBcc(bcc);
			mail.setParameters(parameters);
			mail.setAttachment(attachment);
			if (booking.getAmmendmentFlag() == null) {
				mail.setTemplateNo(activityTypeId);
				parameters.put("destination", destination);
				subjectParameters.put("destination", destination);
				var dateString = booking.getBooking().getPassengers().get(0).getFlights().get(0).getDepartureDate();
				dateString = dateString.substring(0, dateString.indexOf('T'));
				var date = Date.valueOf(dateString);
				var sdf1 = new SimpleDateFormat("MMM dd, yyyy", Locale.ENGLISH);
				var timeString1 = sdf1.format(date);

				subjectParameters.put("date", timeString1);
			} else
				mail.setTemplateNo(propertyConfiguration.getEmailAmmendmentTemplate());

			mail.setSubjectParameters(subjectParameters);
			return sendMailWithAttachment(mail);
		} catch (Exception e) {
			logger.error("sendMailWithAttchement(Mail mailPojo): Exception: ", e);
			return null;
		}
	}

	/**
	 * @param currentData
	 * @return String
	 * 
	 */
	public static String getJsonObject(Upload currentData) {
		String json = null;
		try {
			var objGson = new GsonBuilder().setPrettyPrinting().create();
			return objGson.toJson(currentData);
		} catch (JsonParseException e) {
			logger.error("getJsonObject(Upload currentData): Exception", e);
			return json;
		}

	}
}
