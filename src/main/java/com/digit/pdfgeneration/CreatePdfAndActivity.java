package com.digit.pdfgeneration;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import com.digit.exception.ActivityException;
import com.digit.model.Activity;
import com.digit.model.Booking;
import com.digit.model.ServiceResponse;
import com.digit.utility.PropertyConfiguration;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * 
 * @author Lakshman.Doke
 *
 */

@Component
public class CreatePdfAndActivity {
	@Autowired
	private PropertyConfiguration propertyConfiguration;

	PdfReceivedProcess pdfReceivedProcess = new PdfReceivedProcess();

	private static final Logger logger = LoggerFactory.getLogger(CreatePdfAndActivity.class);

	public void proceed(Booking booking) throws Exception {

		List<Activity> listActivity = PolicyToActivity.convertPolicyToActivity(booking);

		List<Long> activityIdList = new ArrayList<>();
		for (var i = 0; i < listActivity.size(); i++) {
			long activityId = PolicyToActivity.createActivity(listActivity.get(i));
			activityIdList.add(activityId);
		}
		listActivity.clear();
		for (var i = 0; i < activityIdList.size(); i++) {
			var activity = PolicyToActivity.getActivity();
			listActivity.add(activity);
			logger.info("proceed(Booking booking): activity" + activityIdList.get(i));
		}
		logger.info("proceed(Booking booking): created activity...");

		var gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().setPrettyPrinting().create();
		String jsonToSend = gson.toJson(booking, Booking.class);
		logger.info("proceed(Booking booking): Json request : " + jsonToSend);

		ResponseEntity<?> responseEntity = WebClient.create(propertyConfiguration.getPdfGenerateUrl()).post()
				.accept(MediaType.APPLICATION_JSON)
				.header(HttpHeaders.AUTHORIZATION, propertyConfiguration.getPassword()).body(booking, Booking.class)
				.retrieve().toEntity(String.class).block();
		if (responseEntity != null && responseEntity.getStatusCode() != HttpStatus.OK) {
			logger.info("proceed(Booking booking): Response status code " + responseEntity.getStatusCode());
			var res = String.valueOf(responseEntity.getBody());
			logger.info("proceed(Booking booking): Can not create Policy documents");
			for (var i = 0; i < listActivity.size(); i++) {
				var activity = PolicyToActivity.updateActivityData(listActivity.get(i), "", 27);
				PolicyToActivity.updateActivity(activity);
			}
			throw new ActivityException("PDF generation failed" + res);
		} else {
			for (var i = 0; i < listActivity.size(); i++) {
				var activity = PolicyToActivity.updateActivityData(listActivity.get(i), "", 28);
				PolicyToActivity.updateActivity(activity);
			}
			var res = String.valueOf(responseEntity.getBody());
			logger.info("proceed(Booking booking): Response: " + res);
			var receiveGson = new Gson();
			var serviceResponse = receiveGson.fromJson(res, ServiceResponse.class);
			pdfReceivedProcess.proceed(booking, serviceResponse, listActivity);
		}
	}
}
