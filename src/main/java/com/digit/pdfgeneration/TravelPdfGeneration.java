package com.digit.pdfgeneration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.digit.exception.ActivityException;
import com.digit.model.Booking;
/**
 * 
 * @author Lakshman.Doke
 *
 */

public class TravelPdfGeneration {

	private static final Logger logger = LoggerFactory.getLogger(TravelPdfGeneration.class);

	CreatePdfAndActivity process = new CreatePdfAndActivity();

	public void generatePdf(Booking booking) {
		try {
			process.proceed(booking);
		} catch (ActivityException e) {
			logger.error("generatePdf(Booking booking): Exception", e);
		} catch (Exception e) {
			logger.error("generatePdf(Booking booking): Exception", e);
		}
	}
}
