package com.digit.pdfgeneration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.reactive.function.client.WebClient;

import com.digit.exception.UrlShorterException;
import com.digit.utility.PropertyConfiguration;
/**
 * 
 * @author Lakshman.Doke
 *
 */
public class UrlShrotner {
    @Autowired
    private static PropertyConfiguration propertyConfiguration;
    public static String shortenUrl(String url) throws UrlShorterException {
        ResponseEntity<String> responseEntity = WebClient.create(propertyConfiguration.getUrlShortnerUrl())
                .post()
                .accept(MediaType.TEXT_PLAIN)
                .body(url,String.class)
                .header(HttpHeaders.AUTHORIZATION , propertyConfiguration.getPassword())
                .retrieve()
                .toEntity(String.class)
                .block();
        if (responseEntity.getStatusCode() != HttpStatus.OK) {
            throw new UrlShorterException("URL shortner Failed" + responseEntity.getBody().toString());
        } else {
            return responseEntity.getBody();
        }
    }
}
