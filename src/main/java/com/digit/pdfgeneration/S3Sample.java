package com.digit.pdfgeneration;

import com.digit.DigitEncryption;
/**
 * 
 * @author Lakshman.Doke
 *
 */
public class S3Sample {
    public String decrypt(String url) throws Exception {
        String encriptedText = url.substring(url.indexOf('=') + 1, url.length());
        String decriptedText = DigitEncryption.decryptText(encriptedText);
        String[] decriptedArray = decriptedText.split("&");
        return decriptedArray[1].substring(decriptedArray[1].indexOf("=") + 1, decriptedArray[1].length());
    }
}
