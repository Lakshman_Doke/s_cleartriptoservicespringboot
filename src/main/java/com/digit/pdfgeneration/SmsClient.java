package com.digit.pdfgeneration;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.reactive.function.client.WebClient;

import com.digit.entities.Flight;
import com.digit.exception.UrlShorterException;
import com.digit.model.Booking;
import com.digit.model.SmsModel;
import com.digit.utility.PropertyConfiguration;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
/**
 * 
 * @author Lakshman.Doke
 *
 */
public class SmsClient {
    private static final Logger logger = LoggerFactory.getLogger(SmsClient.class);
    @Autowired
    private PropertyConfiguration propertyConfiguration;

    public static String getJsonObject(SmsModel currentData) {
        try {
            Gson objGson = new GsonBuilder().setPrettyPrinting().create();
            SmsModel list1 = currentData;
            return  objGson.toJson(list1);
        } catch (Exception e) {
            logger.error("getJsonObject(SmsModel currentData): Exception Occurred : ",e);
            return new SmsModel().toString();
        }
    }
    public String callSms(String json) {
        String transaction = null;
        logger.info("callSms(String json): ",json);
        try {
           ResponseEntity<String> responseEntity =  WebClient.create(propertyConfiguration.getSmsSendUrl())
                    .post()
                    .accept(MediaType.APPLICATION_JSON)
                    .body(json,String.class)
                    .retrieve()
                    .toEntity(String.class)
                    .block();
        if(responseEntity.getStatusCode() != HttpStatus.OK){
            logger.error("callSms(String json): Sms sending failed");
            return transaction;
        }
            transaction = responseEntity.getBody();
        return  transaction;
        }catch (Exception e){
            logger.error("callSms(String json): Exception:",e);
            return transaction;
        }
    }
    public String sendSms(Booking booking, int flag, String shortenedLink) {
        String transaction = null;
        try {
            SmsModel sm = new SmsModel();
            HashMap<String, String> parameters = new HashMap<String, String>();

            String mobile = null;
            String destination = null;
            if (booking.getAmmendmentFlag() == null) {

                mobile = booking.getBooking().getPassengers().get(0).getMobile();

                parameters.put("name", booking.getBooking().getPassengers().get(0).getFirstName());

                for (int i = 0; i < booking.getBooking().getPassengers().get(0).getFlights().size(); i++) {
                    Flight flight = booking.getBooking().getPassengers().get(0).getFlights().get(i);
                    if (flight.getJourneyType().equalsIgnoreCase("Onward")) {
                        destination = flight.getDestination();
                    }
                }

                parameters.put("destination", destination);
                parameters.put("partner", propertyConfiguration.getPartener());

                try {
                    parameters.put("policyNumber", UrlShrotner.shortenUrl(shortenedLink) + " ");
                } catch (UrlShorterException e) {
                    parameters.put("policyNumber", shortenedLink + " ");
                }

                sm.setTemplateNo(propertyConfiguration.getSmsTemplateNo());

                mobile = ("91" + mobile.substring(mobile.length() - 10, mobile.length()));

                sm.setTo(mobile);
                sm.setParameters(parameters);
                String json = getJsonObject(sm);
                transaction = callSms(json);
            } else {
                if (booking.getBooking().getChannel().equalsIgnoreCase("B2C"))
                    mobile = booking.getBooking().getCustomer().getMobile();
                else if (booking.getBooking().getChannel().equalsIgnoreCase("B2B"))
                    mobile = booking.getBooking().getPassengers().get(0).getMobile();

                if (booking.getBooking().getChannel().equalsIgnoreCase("B2B"))
                    parameters.put("name", booking.getBooking().getPassengers().get(0).getFirstName());
                else
                    parameters.put("name", booking.getBooking().getCustomer().getFirstName());

                sm.setTemplateNo(propertyConfiguration.getEmailAmmendmentTemplate());

                mobile = ("91" + mobile.substring(mobile.length() - 10, mobile.length()));

                sm.setTo(mobile);
                sm.setParameters(parameters);
                String json = getJsonObject(sm);
                transaction = this.callSms(json);
            }
            return transaction;
        }catch (Exception e){
            logger.error("sendSms(Booking booking, int flag, String shortenedLink): Exception Occurred: ",e);
            return transaction;
        }
    }
}
