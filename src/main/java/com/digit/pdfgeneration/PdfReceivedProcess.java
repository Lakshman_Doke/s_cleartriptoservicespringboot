package com.digit.pdfgeneration;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.digit.exception.ActivityException;
import com.digit.model.Activity;
import com.digit.model.Booking;
import com.digit.model.ServiceResponse;
import com.digit.utility.PropertyConfiguration;
/**
 * 
 * @author Lakshman.Doke
 *
 */
public class PdfReceivedProcess {
	@Autowired
	private PropertyConfiguration propertyConfiguration;
	private static final Logger logger = LoggerFactory.getLogger(PdfReceivedProcess.class);

	public void proceed(Booking booking, ServiceResponse serviceResponse, List<Activity> listActivity)
			throws ActivityException {

		List<String> s3Links = new ArrayList<>();
		MailProcess mailProcess = new MailProcess();
		String scheduleLink = null;

		scheduleLink = serviceResponse.getScheduledLink();
		s3Links.add(scheduleLink);

		if (booking.getBooking().getPassengers().get(0).getFlights().get(0).getProduct().equalsIgnoreCase("11"))
			s3Links.add(propertyConfiguration.getSummaryLinkComp());
		else if (booking.getBooking().getPassengers().get(0).getFlights().get(0).getProduct().equalsIgnoreCase("12"))
			s3Links.add(propertyConfiguration.getSummaryLinkArc());

		String transaction = null;
		transaction = mailProcess.getMailObjectAndSendEmail(booking, s3Links, "4");
		SmsClient sms = new SmsClient();
		String smsTransaction = sms.sendSms(booking, 1, scheduleLink);
		if (transaction == null) {
			for (Activity activity : listActivity) {
				Activity updatedActivity = PolicyToActivity.updateActivityData(activity, transaction, 30);
				PolicyToActivity.updateActivity(updatedActivity);
			}
		} else {
			// call update activity
			for (Activity activity : listActivity) {
				Activity updatedActivity = PolicyToActivity.updateActivityData(activity, transaction, 2);
				PolicyToActivity.updateActivity(updatedActivity);
			}
		}
		if (smsTransaction == null) {
			for (Activity value : listActivity) {
				Activity activity = PolicyToActivity.updateActivityData(value, "", 29);
				PolicyToActivity.updateActivity(activity);
			}
		} else {
			for (Activity activity : listActivity) {
				var updatedActivity = PolicyToActivity.updateActivityData(activity, smsTransaction, 3);
				PolicyToActivity.updateActivity(updatedActivity);
			}
		}
		var s3Sample = new S3Sample();
		List<String> fileNameList = new ArrayList<>();
		String scheduleName = null;
		try {
			scheduleName = s3Sample.decrypt(s3Links.get(0));
		} catch (Exception e) {
			logger.error("proceed(Booking booking, ServiceResponse serviceResponse, List<Activity> listActivity): Exception:", e);
		}
		fileNameList.add(scheduleName);
		fileNameList.add("Summary");
		for (Activity activity : listActivity) {
			activity = PolicyToActivity.updateActivityDocumnetWhenPushAbsFailed(activity, s3Links, fileNameList, 31);
			PolicyToActivity.updateActivity(activity);
		}
	}

}	
