package com.digit.pdfgeneration;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;

import com.digit.entities.Passenger;
import com.digit.exception.ActivityException;
import com.digit.model.Activity;
import com.digit.model.ActivityAdditionalDetails;
import com.digit.model.ActivityStatusChange;
import com.digit.model.Booking;
import com.digit.model.DocumentDetails;
import com.digit.model.Person;
import com.digit.model.PersonCommunication;
import com.digit.model.UserCredentials;
import com.digit.utility.PropertyConfiguration;
import com.google.gson.GsonBuilder;

import reactor.core.publisher.Mono;
/**
 * 
 * @author Lakshman.Doke
 *
 */
public class PolicyToActivity {

	@Value("${DB_ID_KEY}")
	private static long BATCH_USER_ID;

	@Value("${ACTIVITY_TYPE_ID_MAIL_TRAVEL}")
	private static int TRAVEL_CLAIM_BATCH_ACTIVITY_TYPE;

	@Value("${PRODUCT_ID}")
	private static int TRAVEL_PRODUCT_ID;

	@Value("${COMMUNICATION_TYPE_MOBILE}")
	private static int COMMUNICATION_TYPE_MOBILE;

	@Value("${COMMUNICATION_TYPE_EMAIL}")
	private static int COMMUNICATION_TYPE_EMAIL;

	@Value("${COMMUNICATION_SUBTYPE_PRIMARY}")
	private static int COMMUNICATION_SUBTYPE_PRIMARY;

	@Value("${ABS_LOGICAL_SECTION_TRAVEL}")
	public static String LOGICAL_TRAVEL;

	@Value("${ACTIVITY_URL}")
	public static String ACTIVITY_URL;

	@Autowired
	private static PropertyConfiguration propertyConfiguration;

	private static final Logger logger = LoggerFactory.getLogger(PolicyToActivity.class);

    public static List<Activity> convertPolicyToActivity(Booking booking) {
		var creationDate = new Timestamp(System.currentTimeMillis());
		List<Activity> activityList = new ArrayList<>();
		List<ActivityAdditionalDetails> listActivityAdditionalDetails = new ArrayList<>();
		long user = BATCH_USER_ID;

		for (var j = 0; j < booking.getBooking().getPassengers().size(); j++) {

			if (booking.getBooking().getPassengers().get(j).getFlights().get(0).getDIGITPolicyNo().trim().isEmpty()
					|| booking.getBooking().getPassengers().get(j).getFlights().get(0).getDIGITPolicyNo()
							.equalsIgnoreCase("NA"))
				continue;

			var activity = new Activity();
			// ----------------------------------
			activity.setCreatedUserId(user);
			activity.setLastModifiedUserId(user);
			activity.setCreatedDate(creationDate);
			activity.setLastModifiedDate(creationDate);
			activity.setActivityDate(creationDate);
			activity.setVersionNumber(1);

			String policyNumber = booking.getBooking().getPassengers().get(j).getFlights().get(0).getDIGITPolicyNo();

			activity.setPolicyNumber(policyNumber);
			activity.setStatus(1);

			activity.setActivityTypeId(TRAVEL_CLAIM_BATCH_ACTIVITY_TYPE);
			activity.setProductId(TRAVEL_PRODUCT_ID);
			// -----------------------------------

			var person = new Person();
			getPerson(person, booking.getBooking().getPassengers().get(j));
			activity.setPerson(person);

			// ---------------------------------------
			for (var i = 0; i < booking.getBooking().getPassengers().get(j).getFlights().size(); i++) {

				String journeyType = booking.getBooking().getPassengers().get(j).getFlights().get(i).getJourneyType();
				String leg = booking.getBooking().getPassengers().get(j).getFlights().get(i).getLeg();
				var activityAdditionalDetails1 = new ActivityAdditionalDetails();
				activityAdditionalDetails1.setCreatedDate(creationDate);
				activityAdditionalDetails1.setCreatedUserId(user);
				activityAdditionalDetails1.setLastModifiedDate(creationDate);
				activityAdditionalDetails1.setLastModifiedUserId(user);
				activityAdditionalDetails1.setKey("Airline " + journeyType + " " + leg);
				activityAdditionalDetails1
						.setValue(booking.getBooking().getPassengers().get(j).getFlights().get(i).getCarrierName());

				var activityAdditionalDetails2 = new ActivityAdditionalDetails();
				activityAdditionalDetails2.setCreatedDate(creationDate);
				activityAdditionalDetails2.setCreatedUserId(user);
				activityAdditionalDetails2.setLastModifiedDate(creationDate);
				activityAdditionalDetails2.setLastModifiedUserId(user);
				activityAdditionalDetails2.setKey("FlightNo " + journeyType + " " + leg);
				String flightno = booking.getBooking().getPassengers().get(j).getFlights().get(i).getCarrierNo()
						+ booking.getBooking().getPassengers().get(j).getFlights().get(i).getCarrierFlightNo();
				activityAdditionalDetails2.setValue(flightno);

				var activityAdditionalDetails3 = new ActivityAdditionalDetails();
				activityAdditionalDetails3.setCreatedDate(creationDate);
				activityAdditionalDetails3.setCreatedUserId(user);
				activityAdditionalDetails3.setLastModifiedDate(creationDate);
				activityAdditionalDetails3.setLastModifiedUserId(user);
				activityAdditionalDetails3.setKey("Pnr  " + journeyType + " " + leg);
				activityAdditionalDetails3
						.setValue(booking.getBooking().getPassengers().get(j).getFlights().get(i).getAirlinePNR());

				var activityAdditionalDetails4 = new ActivityAdditionalDetails();
				activityAdditionalDetails4.setCreatedDate(creationDate);
				activityAdditionalDetails4.setCreatedUserId(user);
				activityAdditionalDetails4.setLastModifiedDate(creationDate);
				activityAdditionalDetails4.setLastModifiedUserId(user);
				activityAdditionalDetails4.setKey("TravelDate " + journeyType + " " + leg);
				String date = booking.getBooking().getPassengers().get(j).getFlights().get(i).getDepartureDate()
						.split("T")[0];
				activityAdditionalDetails4.setValue(date);

				var activityAdditionalDetails5 = new ActivityAdditionalDetails();
				activityAdditionalDetails5.setCreatedDate(creationDate);
				activityAdditionalDetails5.setCreatedUserId(user);
				activityAdditionalDetails5.setLastModifiedDate(creationDate);
				activityAdditionalDetails5.setLastModifiedUserId(user);
				activityAdditionalDetails5.setKey("ScheduledDeparture " + journeyType + " " + leg);
				var time = "";
				if (booking.getBooking().getPassengers().get(j).getFlights().get(i).getDepartureDate().contains("T"))
					time = booking.getBooking().getPassengers().get(j).getFlights().get(i).getDepartureDate()
							.split("T")[1];
				else if (booking.getBooking().getPassengers().get(j).getFlights().get(i).getDepartureDate()
						.contains(" "))
					time = booking.getBooking().getPassengers().get(j).getFlights().get(i).getDepartureDate()
							.split(" ")[1];
				activityAdditionalDetails5.setValue(time);

				var activityAdditionalDetails6 = new ActivityAdditionalDetails();
				activityAdditionalDetails6.setCreatedDate(creationDate);
				activityAdditionalDetails6.setCreatedUserId(user);
				activityAdditionalDetails6.setLastModifiedDate(creationDate);
				activityAdditionalDetails6.setLastModifiedUserId(user);
				activityAdditionalDetails6.setKey("Source " + journeyType + " " + leg);
				activityAdditionalDetails6
						.setValue(booking.getBooking().getPassengers().get(j).getFlights().get(i).getOrigin());

				var activityAdditionalDetails7 = new ActivityAdditionalDetails();
				activityAdditionalDetails7.setCreatedDate(creationDate);
				activityAdditionalDetails7.setCreatedUserId(user);
				activityAdditionalDetails7.setLastModifiedDate(creationDate);
				activityAdditionalDetails7.setLastModifiedUserId(user);
				activityAdditionalDetails7.setKey("Destination " + journeyType + " " + leg);
				activityAdditionalDetails7
						.setValue(booking.getBooking().getPassengers().get(j).getFlights().get(i).getDestination());

				var activityAdditionalDetails8 = new ActivityAdditionalDetails();
				activityAdditionalDetails8.setCreatedDate(creationDate);
				activityAdditionalDetails8.setCreatedUserId(user);
				activityAdditionalDetails8.setLastModifiedDate(creationDate);
				activityAdditionalDetails8.setLastModifiedUserId(user);
				activityAdditionalDetails8.setKey("cabinType " + journeyType + " " + leg);
				activityAdditionalDetails8
						.setValue(booking.getBooking().getPassengers().get(j).getFlights().get(i).getCabinType());

				var activityAdditionalDetails9 = new ActivityAdditionalDetails();
				activityAdditionalDetails9.setCreatedDate(creationDate);
				activityAdditionalDetails9.setCreatedUserId(user);
				activityAdditionalDetails9.setLastModifiedDate(creationDate);
				activityAdditionalDetails9.setLastModifiedUserId(user);
				activityAdditionalDetails9.setKey("journeyType " + journeyType + " " + leg);
				activityAdditionalDetails9
						.setValue(booking.getBooking().getPassengers().get(j).getFlights().get(i).getJourneyType());

				var activityAdditionalDetails10 = new ActivityAdditionalDetails();
				activityAdditionalDetails10.setCreatedDate(creationDate);
				activityAdditionalDetails10.setCreatedUserId(user);
				activityAdditionalDetails10.setLastModifiedDate(creationDate);
				activityAdditionalDetails10.setLastModifiedUserId(user);
				activityAdditionalDetails10.setKey("leg " + journeyType + " " + leg);
				activityAdditionalDetails10
						.setValue(booking.getBooking().getPassengers().get(j).getFlights().get(i).getLeg());

				var activityAdditionalDetails11 = new ActivityAdditionalDetails();
				activityAdditionalDetails11.setCreatedDate(creationDate);
				activityAdditionalDetails11.setCreatedUserId(user);
				activityAdditionalDetails11.setLastModifiedDate(creationDate);
				activityAdditionalDetails11.setLastModifiedUserId(user);
				activityAdditionalDetails11.setKey("updateType " + journeyType + " " + leg);
				activityAdditionalDetails11
						.setValue(booking.getBooking().getPassengers().get(j).getFlights().get(i).getUpdateType());

				var activityAdditionalDetails12 = new ActivityAdditionalDetails();
				activityAdditionalDetails12.setCreatedDate(creationDate);
				activityAdditionalDetails12.setCreatedUserId(user);
				activityAdditionalDetails12.setLastModifiedDate(creationDate);
				activityAdditionalDetails12.setLastModifiedUserId(user);
				activityAdditionalDetails12.setKey("channel " + journeyType + " " + leg);
				activityAdditionalDetails12.setValue(booking.getBooking().getChannel());

				var activityAdditionalDetails14 = new ActivityAdditionalDetails();
				activityAdditionalDetails14.setCreatedDate(creationDate);
				activityAdditionalDetails14.setCreatedUserId(user);
				activityAdditionalDetails14.setLastModifiedDate(creationDate);
				activityAdditionalDetails14.setLastModifiedUserId(user);
				activityAdditionalDetails14.setKey("bookingRef " + journeyType + " " + leg);
				activityAdditionalDetails14.setValue(booking.getBooking().getTripId());

				listActivityAdditionalDetails.add(activityAdditionalDetails1);
				listActivityAdditionalDetails.add(activityAdditionalDetails2);
				listActivityAdditionalDetails.add(activityAdditionalDetails3);
				listActivityAdditionalDetails.add(activityAdditionalDetails4);
				listActivityAdditionalDetails.add(activityAdditionalDetails5);
				listActivityAdditionalDetails.add(activityAdditionalDetails6);
				listActivityAdditionalDetails.add(activityAdditionalDetails7);
				listActivityAdditionalDetails.add(activityAdditionalDetails8);
				listActivityAdditionalDetails.add(activityAdditionalDetails9);
				listActivityAdditionalDetails.add(activityAdditionalDetails10);
				listActivityAdditionalDetails.add(activityAdditionalDetails11);
				listActivityAdditionalDetails.add(activityAdditionalDetails12);
				listActivityAdditionalDetails.add(activityAdditionalDetails14);
			}
			activity.setAdditionalActivitiesList(listActivityAdditionalDetails);

			// ----------------------------------------------------------------------------
			var activityStatusChange = new ActivityStatusChange();
			activityStatusChange.setCreatedDate(creationDate);
			activityStatusChange.setCreatedUseId(user);
			activityStatusChange.setLastModifiedDate(creationDate);
			activityStatusChange.setLastModifiedUserId(user);
			activityStatusChange.setDateofChange(creationDate);
			activityStatusChange.setStatus(1);

			List<ActivityStatusChange> listActivityStatusChange = new ArrayList<>();
			listActivityStatusChange.add(activityStatusChange);
			activity.setStatusActivities(listActivityStatusChange);

			// -----------------------------------------------------------------------------
			activityList.add(activity);

		}
		return activityList;
	}

	public static Activity updateActivityData(Activity activity, String transaction, int status) {
		long user = BATCH_USER_ID;
		var modificationDate = new Timestamp(System.currentTimeMillis());

		activity.setLastModifiedUserId(user);
		activity.setLastModifiedDate(modificationDate);
		activity.setStatus(status);

		List<ActivityStatusChange> listActivityStatusChangeUpdate = new ArrayList<>();

		var activityStatusChange = new ActivityStatusChange();

		activityStatusChange.setCreatedDate(modificationDate);
		activityStatusChange.setLastModifiedDate(modificationDate);
		activityStatusChange.setLastModifiedUserId(user);
		activityStatusChange.setDateofChange(modificationDate);
		activityStatusChange.setStatus(status);
		activityStatusChange.setTransactionNumber(transaction);
		activityStatusChange.setCreatedUseId(user);
		listActivityStatusChangeUpdate.add(activityStatusChange);

		activity.setStatusActivities(listActivityStatusChangeUpdate);

		return activity;
	}

	public static Activity updateActivityDocumnet(Activity activity, List<String> attchemtLinks,
			List<String> absDocumentLinks, List<String> fileNameList, int status) {
		long user = BATCH_USER_ID;
		var modificationDate = new Timestamp(System.currentTimeMillis());

		activity.setLastModifiedUserId(user);
		activity.setLastModifiedDate(modificationDate);
		activity.setStatus(status);
		List<DocumentDetails> list_document = new ArrayList<>();
        for (var i = 0; i < absDocumentLinks.size(); i++) {
			String doc = absDocumentLinks.get(i);
			String link = attchemtLinks.get(i);
			String document = fileNameList.get(i);
			var documentsDetails = new DocumentDetails();
			documentsDetails.setLastModifiedDate(modificationDate);
			documentsDetails.setCreatedDate(modificationDate);
			documentsDetails.setLastModifiedUserId(user);
			documentsDetails.setCreatedUserId(user);
			documentsDetails.setABSDocumentKey(doc);
			documentsDetails.setDocumentName(document);
			documentsDetails.setDocumentType(1);
			documentsDetails.setS3Link(link);
			list_document.add(documentsDetails);
		}
		activity.setDocumentDetails(list_document);

		return activity;
	}

	public static Activity updateActivityDocumnetWhenPushAbsFailed(Activity activity, List<String> attchemtLinks,
			List<String> fileNameList, int status) {
		long user = BATCH_USER_ID;
		var modificationDate = new Timestamp(System.currentTimeMillis());

		activity.setLastModifiedUserId(user);
		activity.setLastModifiedDate(modificationDate);
		activity.setStatus(status);
		List<ActivityStatusChange> listActivityStatusChangeUpdate = new ArrayList<>();

		var activityStatusChange = new ActivityStatusChange();

		activityStatusChange.setCreatedDate(modificationDate);
		activityStatusChange.setLastModifiedDate(modificationDate);
		activityStatusChange.setLastModifiedUserId(user);
		activityStatusChange.setDateofChange(modificationDate);
		activityStatusChange.setStatus(status);
		activityStatusChange.setCreatedUseId(user);
		listActivityStatusChangeUpdate.add(activityStatusChange);

		activity.setStatusActivities(listActivityStatusChangeUpdate);
		List<DocumentDetails> list_document = new ArrayList<>();

		List<String> linkList = attchemtLinks;
		List<String> documentNames = fileNameList;
		for (var i = 0; i < linkList.size(); i++) {

			String link = linkList.get(i);
			String document = documentNames.get(i);
			var documentsDetails = new DocumentDetails();
			documentsDetails.setLastModifiedDate(modificationDate);
			documentsDetails.setCreatedDate(modificationDate);
			documentsDetails.setLastModifiedUserId(user);
			documentsDetails.setCreatedUserId(user);
			documentsDetails.setABSDocumentKey(null);
			documentsDetails.setDocumentName(document);
			documentsDetails.setDocumentType(1);
			documentsDetails.setS3Link(link);
			list_document.add(documentsDetails);
		}
		activity.setDocumentDetails(list_document);

		return activity;
	}

	public static long createActivity(Activity activity) throws ActivityException {
		String authorization = getAuth();
		var stringActivity = getJsonObject(activity);
		logger.info("createActivity(Activity activity): ", stringActivity);
		ResponseEntity<?> responseEntity = WebClient.create(ACTIVITY_URL).post().accept(MediaType.TEXT_HTML)
				.header(HttpHeaders.AUTHORIZATION, authorization).body(activity, Activity.class).retrieve()
				.toEntity(String.class).block();
		if (responseEntity.getStatusCode() != HttpStatus.OK) {
			logger.error("createActivity(Activity activity): Can not create Activity..", responseEntity);
			throw new ActivityException("Activity creation failed" + responseEntity);
		}
		logger.info("createActivity(Activity activity): Activity created with id:-"
				+ Long.valueOf(responseEntity.getBody().toString()));
		return Long.valueOf(responseEntity.getBody().toString());
	}
	// ---------update existing activity------------------

	public static void updateActivity(Activity activity) throws ActivityException {
		String authorization = getAuth();
		ResponseEntity<?> responseEntity = WebClient.create(ACTIVITY_URL).put().accept(MediaType.TEXT_HTML)
				.header(HttpHeaders.AUTHORIZATION, authorization).body(activity, Activity.class).retrieve()
				.toEntity(String.class).block();
		if (responseEntity.getStatusCode() != HttpStatus.OK) {
			throw new ActivityException(responseEntity.getBody().toString());
		}
		logger.info(String.format("updateActivity(Activity activity): updated activity id:- '{0} ",activity.getActivityID()));

	}
	// ------------------------------------------------------

	public static Activity getActivity() {
		String authorization = getAuth();
		return WebClient.create(ACTIVITY_URL).get().accept(MediaType.APPLICATION_JSON)
				.header(HttpHeaders.AUTHORIZATION, authorization).retrieve()
				.onStatus(status -> !status.is2xxSuccessful(), ClientResponse::createException)
				.toEntity(Activity.class).block().getBody();
	}

	public static String getJsonObject(Activity currentData) {
		var objGson = new GsonBuilder().setPrettyPrinting().create();
        return objGson.toJson(currentData);
	}

    public static void getPerson(Person person, Passenger passenger) {
		var creationDate = new Timestamp(System.currentTimeMillis());
		long user = BATCH_USER_ID;
		person.setCreatedDate(creationDate);
		person.setLastModifiedDate(creationDate);
		person.setCreatedUserId(user);
		person.setLastModifiedUserId(user);
		person.setFirstName(passenger.getFirstName());
		person.setLastName(passenger.getLastName());

		// -------------------------------------

		var personCommunication1 = new PersonCommunication();
		personCommunication1.setCreatedDate(creationDate);
		personCommunication1.setLastModifiedDate(creationDate);
		personCommunication1.setCreatedUserId(user);
		personCommunication1.setLastModifiedUserId(user);
		personCommunication1.setCommunicationType(COMMUNICATION_TYPE_MOBILE);
		personCommunication1.setCommunicationValue(passenger.getMobile());
		personCommunication1.setSubType(COMMUNICATION_SUBTYPE_PRIMARY);
		personCommunication1.setActive(true);

		var personCommunication2 = new PersonCommunication();
		personCommunication2.setCreatedDate(creationDate);
		personCommunication2.setLastModifiedDate(creationDate);
		personCommunication2.setCreatedUserId(user);
		personCommunication2.setLastModifiedUserId(user);
		personCommunication2.setCommunicationType(COMMUNICATION_TYPE_EMAIL);
		personCommunication2.setCommunicationValue(passenger.getEmail());
		personCommunication2.setSubType(COMMUNICATION_SUBTYPE_PRIMARY);
		personCommunication2.setActive(true);
		// --------------------------------------

		List<PersonCommunication> listPersonCommunication = new ArrayList<>();
		listPersonCommunication.add(personCommunication1);
		listPersonCommunication.add(personCommunication2);

		person.setPersonCommunications(listPersonCommunication);
	}

    public static String getAuth() {
        var user = new UserCredentials(BATCH_USER_ID, propertyConfiguration.getPassword());
        logger.info("getAuth(): Authorization initiate......");

        return WebClient.builder().build().post()
                .uri(propertyConfiguration.getAuthUrl(), uriBuilder -> uriBuilder.path("{id}").build(2)).accept(MediaType.TEXT_HTML).body(Mono.just(user), UserCredentials.class).retrieve()
                .onStatus(httpStatus -> !httpStatus.is2xxSuccessful(), ClientResponse::createException)
                .toEntity(String.class).block().getHeaders().get(HttpHeaders.AUTHORIZATION).get(0);
    }
}
