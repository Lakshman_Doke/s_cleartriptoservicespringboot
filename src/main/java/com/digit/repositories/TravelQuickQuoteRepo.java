package com.digit.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.digit.entities.TravelQuickQuote;
import com.digit.entities.TravelQuickQuoteId;
/**
 * 
 * @author Lakshman.Doke
 *
 */
@Repository
public interface TravelQuickQuoteRepo extends JpaRepository<TravelQuickQuote, TravelQuickQuoteId> {
	
	List<TravelQuickQuote> findByTravelQuickQuoteId(TravelQuickQuoteId travelQuickQuoteId);
}
