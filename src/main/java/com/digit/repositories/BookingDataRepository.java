package com.digit.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.digit.entities.BookingData;
/**
 * 
 * @author Lakshman.Doke
 *
 */
@Repository
public interface BookingDataRepository extends JpaRepository<BookingData, Long> {

	@Query("SELECT bd FROM BookingData bd where bd.TripId = :tripId and bd.subInsuranceProductCode = :subInsuranceProductCode order by bd.bookingDataId desc")
	List<BookingData> getBookingDataByTripIdAndSubInsuranceProductCode(@Param("tripId") String tripId,
			@Param("subInsuranceProductCode") String subInsuranceProductCode);

	List<BookingData> findByisClaimTriggerSuccessful(Boolean value);
	@Query(nativeQuery = true , value = "select distinct(b.bookingdataid) from digit_travel.t_booking_data b join digit_travel.t_passenger p on b.bookingdataid=p.bookingdataid where b.created_date > '2019-06-15' and (p.policynumber is null or p.policynumber='NA');")
	List<Long> getMissedBookingsId();
}
