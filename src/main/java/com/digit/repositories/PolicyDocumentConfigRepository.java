package com.digit.repositories;

import com.digit.entities.PolicyDocumentConfig;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
/**
 * 
 * @author Lakshman.Doke
 *
 */
@Repository
public interface PolicyDocumentConfigRepository extends JpaRepository<PolicyDocumentConfig, Long> {
    List<PolicyDocumentConfig> findByActiveAndProductOfferingCodeAndPdfSourceNotAndAmendmentCode(
            Boolean active, String productOfferingCode, String pdfSource, String amendmentCode);
}
