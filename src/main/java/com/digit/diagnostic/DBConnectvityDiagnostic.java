package com.digit.diagnostic;
/**
 * 
 * @author Lakshman.Doke
 *
 */
public class DBConnectvityDiagnostic {
    private String DBConnectivity;
    private String status;

    public String getDBConnectivity() {
        return DBConnectivity;
    }

    public void setDBConnectivity(String dBConnectivity) {
        DBConnectivity = dBConnectivity;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
