package com.digit.diagnostic;

import lombok.Data;
/**
 * 
 * @author Lakshman.Doke
 *
 */
@Data
public class ABSDiagnostic {
    private String message;
    private String status;
}
