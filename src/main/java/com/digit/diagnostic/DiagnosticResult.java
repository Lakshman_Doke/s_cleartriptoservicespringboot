package com.digit.diagnostic;
/**
 * 
 * @author Lakshman.Doke
 *
 */
public class DiagnosticResult {
    private String file_temp_location;
    private ABSDiagnostic absDiagnostic;
    private DBConnectvityDiagnostic dataBaseConnectivity;
    private LocationAccess locationAccess;

    public ABSDiagnostic getAbsDiagnostic() {
        return absDiagnostic;
    }

    public void setAbsDiagnostic(ABSDiagnostic absDiagnostic) {
        this.absDiagnostic = absDiagnostic;
    }

    public LocationAccess getLocationAccess() {
        return locationAccess;
    }

    public void setLocationAccess(LocationAccess locationAccess) {
        this.locationAccess = locationAccess;
    }

    public DBConnectvityDiagnostic getDataBaseConnectivity() {
        return dataBaseConnectivity;
    }

    public void setDataBaseConnectivity(DBConnectvityDiagnostic dataBaseConnectivity) {
        this.dataBaseConnectivity = dataBaseConnectivity;
    }

    public String getFile_temp_location() {
        return file_temp_location;
    }

    public void setFile_temp_location(String file_temp_location) {
        this.file_temp_location = file_temp_location;
    }
}
