package com.digit.diagnostic;

import lombok.Data;

import java.io.File;
/**
 * 
 * @author Lakshman.Doke
 *
 */
@Data
public class LocationAccess {
    private String location;
    private File file;
    private String status;
    private String fileCreated;
    private String fileDeleted;
}
