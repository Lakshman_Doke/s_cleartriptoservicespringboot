package com.digit.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.digit.entities.TravelQuickQuote;
import com.digit.entities.TravelQuickQuoteId;
import com.digit.repositories.TravelQuickQuoteRepo;
import com.digit.service.QuickQuoteService;
import com.digit.utility.AbsDataServiceClient;
/**
 * 
 * @author Lakshman.Doke
 *
 */
public class QuickQuoteServiceImpl implements QuickQuoteService {
	@Autowired
	TravelQuickQuoteRepo travelQuickQuoteRepo;

	private static final Logger logger = LoggerFactory.getLogger(AbsDataServiceClient.class);

	@Override
	public TravelQuickQuote getQuickQuoteById(String quickQuoteId, String airline) {
		TravelQuickQuote returnQuickQuote = null;
		TravelQuickQuoteId travelQuickQuoteId = new TravelQuickQuoteId();
		travelQuickQuoteId.setAirline(airline);
		travelQuickQuoteId.setQuickQuoteId(quickQuoteId);
		try {
			List<TravelQuickQuote> quickQuoteList = travelQuickQuoteRepo.findByTravelQuickQuoteId(travelQuickQuoteId);
			if (quickQuoteList != null && quickQuoteList.isEmpty())
				returnQuickQuote = quickQuoteList.get(0);
		} catch (Exception e) {
			logger.error("Failed to get quote details due to " + e.getMessage());
		}
		return returnQuickQuote;
	}

}
