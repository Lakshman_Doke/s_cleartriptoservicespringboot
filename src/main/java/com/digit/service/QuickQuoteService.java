package com.digit.service;

import com.digit.entities.TravelQuickQuote;
/**
 * 
 * @author Lakshman.Doke
 *
 */
public interface QuickQuoteService {
	public TravelQuickQuote getQuickQuoteById(String quickQuoteId, String airline);
}
