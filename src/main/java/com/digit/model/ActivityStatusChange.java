package com.digit.model;

import java.sql.Timestamp;

/**
 * 
 * @author Lakshman.Doke
 *
 */
public class ActivityStatusChange {
	long actStatusChangeId;
	// long activityId;
	long status;
	Timestamp dateofChange;
	String transactionNumber;
	Timestamp createdDate;
	long createdUserId;
	Timestamp lastModifiedDate;
	long lastModifiedUserId;

	private Activity activity;

	private User lastModifiedUser;
	private User createdUser;

	public long getActStatusChangeId() {
		return actStatusChangeId;
	}

	public void setActStatusChangeId(long actStatusChangeId) {
		this.actStatusChangeId = actStatusChangeId;
	}

//	public long getActivityId() {
//		return activityId;
//	}
//
//	public void setActivityId(long activityId) {
//		this.activityId = activityId;
//	}

	public long getStatus() {
		return status;
	}

	public Activity getActivity() {
		return activity;
	}

	public void setActivity(Activity activity) {
		this.activity = activity;
	}

	public void setStatus(long status) {
		this.status = status;
	}

	public Timestamp getDateofChange() {
		return dateofChange;
	}

	public void setDateofChange(Timestamp dateofChange) {
		this.dateofChange = dateofChange;
	}

	public String getTransactionNumber() {
		return transactionNumber;
	}

	public void setTransactionNumber(String transactionNumber) {
		this.transactionNumber = transactionNumber;
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public long getCreatedUserId() {
		return createdUserId;
	}

	public void setCreatedUseId(long createdUserId) {
		this.createdUserId = createdUserId;
	}

	public Timestamp getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Timestamp lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public long getLastModifiedUserId() {
		return lastModifiedUserId;
	}

	public void setLastModifiedUserId(long lastModifiedUserId) {
		this.lastModifiedUserId = lastModifiedUserId;
	}

	public User getLastModifiedUser() {
		return lastModifiedUser;
	}

	public void setLastModifiedUser(User lastModifiedUser) {
		this.lastModifiedUser = lastModifiedUser;
	}

	public User getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(User createdUser) {
		this.createdUser = createdUser;
	}
}
