package com.digit.model;

import java.sql.Timestamp;
import java.util.List;

import lombok.Data;
/**
 * 
 * @author Lakshman.Doke
 *
 */
@Data
public class Role {
	private long role_id;
	private long role_name;
	private Boolean active;
	private Timestamp createdDate;
	private long createdUserId;
	private Timestamp lastModifiedDate;
	private long lastModifiedUserId;
	private List<UserRole> userroles;
	private User createdUser;
	private User lastModifiedUser;
}
