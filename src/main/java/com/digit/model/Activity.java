package com.digit.model;

import java.sql.Timestamp;
import java.util.List;

/**
 * 
 * @author Lakshman.Doke
 *
 */
public class Activity {
	private long activityID;
	private Timestamp activityDate;
	private long agentId;
	private long productId;
	private long activityTypeId;
	private String policyNumber;
	private String claimNumber;
	private long versionNumber;
	private long status;
	private Timestamp createdDate;
	private long lastModifiedUserId;

	private long createdUserId;
	private Timestamp lastModifiedDate;

	private List<ActivityAdditionalDetails> additionalActivitiesList;
	private List<ActivityReceivedDetails> receivedActivities;
	private List<ActivityStatusChange> statusActivities;
	private Person person;
	private List<DocumentDetails> documentDetails;

	private User lastModifiedUser;

	private User createdUser;

	public long getLastModifiedUserId() {
		return lastModifiedUserId;
	}

	public void setLastModifiedUserId(long lastModifiedUserId) {
		this.lastModifiedUserId = lastModifiedUserId;
	}

	public User getLastModifiedUser() {
		return lastModifiedUser;
	}

	public void setLastModifiedUser(User lastModifiedUser) {
		this.lastModifiedUser = lastModifiedUser;
	}

	public User getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(User createdUser) {
		this.createdUser = createdUser;
	}

	public List<ActivityReceivedDetails> getReceivedActivities() {
		return receivedActivities;
	}

	public void setReceivedActivities(List<ActivityReceivedDetails> receivedActivities) {
		this.receivedActivities = receivedActivities;
	}

	public List<ActivityStatusChange> getStatusActivities() {
		return statusActivities;
	}

	public void setStatusActivities(List<ActivityStatusChange> statusActivities) {
		this.statusActivities = statusActivities;
	}

	public Person getPerson() {
		return person;
	}

	public List<ActivityAdditionalDetails> getAdditionalActivitiesList() {
		return additionalActivitiesList;
	}

	public void setAdditionalActivitiesList(List<ActivityAdditionalDetails> additionalActivitiesList) {
		this.additionalActivitiesList = additionalActivitiesList;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public long getActivityID() {
		return activityID;
	}

	public void setActivityID(long activityID) {
		this.activityID = activityID;
	}

	public Timestamp getActivityDate() {
		return activityDate;
	}

	public void setActivityDate(Timestamp activityDate) {
		this.activityDate = activityDate;
	}

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public long getActivityTypeId() {
		return activityTypeId;
	}

	public void setActivityTypeId(long activityTypeId) {
		this.activityTypeId = activityTypeId;
	}

	public String getPolicyNumber() {
		return policyNumber;
	}

	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}

	public String getClaimNumber() {
		return claimNumber;
	}

	public void setClaimNumber(String claimNumber) {
		this.claimNumber = claimNumber;
	}

	public long getVersionNumber() {
		return versionNumber;
	}

	public void setVersionNumber(long versionNumber) {
		this.versionNumber = versionNumber;
	}

	public long getStatus() {
		return status;
	}

	public void setStatus(long status) {
		this.status = status;
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public long getCreatedUserId() {
		return createdUserId;
	}

	public void setCreatedUserId(long createdUserId) {
		this.createdUserId = createdUserId;
	}

	public Timestamp getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Timestamp lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public long getAgentId() {
		return agentId;
	}

	public void setAgentId(long agentId) {
		this.agentId = agentId;
	}

	public List<DocumentDetails> getDocumentDetails() {
		return documentDetails;
	}

	public void setDocumentDetails(List<DocumentDetails> documentDetails) {
		this.documentDetails = documentDetails;
	}

}
