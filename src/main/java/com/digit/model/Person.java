package com.digit.model;

import java.sql.Timestamp;
import java.util.List;
/**
 * 
 * @author Lakshman.Doke
 *
 */
public class Person {
	long personId;
	String absPersonId;
	String lastName;
	String firstName;
	Timestamp createdDate;
	long createdUserId;
	Timestamp lastModifiedDate;
	long lastModifiedUserId;
	private List<Activity> activities;
	private List<PersonCommunication> personCommunications;
	private User lastModifiedUser;
	private User createdUser;

	public List<PersonCommunication> getPersonCommunications() {
		return personCommunications;
	}

	public void setPersonCommunications(List<PersonCommunication> personCommunications) {
		this.personCommunications = personCommunications;
	}

	public List<Activity> getActivities() {
		return activities;
	}

	public void setActivities(List<Activity> activities) {
		this.activities = activities;
	}

	public long getPersonId() {
		return personId;
	}

	public void setPersonId(long personId) {
		this.personId = personId;
	}

	public String getAbsPersonId() {
		return absPersonId;
	}

	public void setAbsPersonId(String absPersonId) {
		this.absPersonId = absPersonId;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public long getCreatedUserId() {
		return createdUserId;
	}

	public void setCreatedUserId(long createdUserId) {
		this.createdUserId = createdUserId;
	}

	public Timestamp getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Timestamp lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public long getLastModifiedUserId() {
		return lastModifiedUserId;
	}

	public void setLastModifiedUserId(long lastModifiedUserId) {
		this.lastModifiedUserId = lastModifiedUserId;
	}

	public User getLastModifiedUser() {
		return lastModifiedUser;
	}

	public void setLastModifiedUser(User lastModifiedUser) {
		this.lastModifiedUser = lastModifiedUser;
	}

	public User getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(User createdUser) {
		this.createdUser = createdUser;
	}

	/*
	 * public List<Activity> getActivities() { return activities; } public void
	 * setActivities(List<Activity> activities) { this.activities = activities; }
	 */
	/*
	 * public List<PersonCommunication> getCommunications() { return communications;
	 * } public void setCommunications(List<PersonCommunication> communications) {
	 * this.communications = communications;
	 * 
	 * }
	 *//*
		 * public String toString() {
		 * 
		 * return "[PersonId:\""+personId+"\",AbsPersonId"+absPersonId+",LastName:"+
		 * lastName+
		 * ",FirstName:"+firstName+",CreatedDate:"+createdDate+",CreatedUser:"+
		 * createdUser+",LastModifiedDate:"+lastModifiedDate+
		 * ",LastModifiedUser:"+lastModifiedUser+"]"; }
		 */
}
