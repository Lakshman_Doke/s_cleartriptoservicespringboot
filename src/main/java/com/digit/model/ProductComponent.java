package com.digit.model;

import java.util.List;
/**
 * 
 * @author Lakshman.Doke
 *
 */
public class ProductComponent {
	private String sumAmount;
	private String productComponentNumber;
	private transient String coverageMapping;
	private List<SubProductComponent> subProductComponents;

	public String getSumAmount() {
		return sumAmount;
	}

	public void setSumAmount(String sumAmount) {
		this.sumAmount = sumAmount;
	}

	public String getProductComponentNumber() {
		return productComponentNumber;
	}

	public void setProductComponentNumber(String productComponentNumber) {
		this.productComponentNumber = productComponentNumber;
	}

	public String getCoverageMapping() {
		return coverageMapping;
	}

	public void setCoverageMapping(String coverageMapping) {
		this.coverageMapping = coverageMapping;
	}

	public List<SubProductComponent> getSubProductComponents() {
		return subProductComponents;
	}

	public void setSubProductComponents(List<SubProductComponent> subProductComponents) {
		this.subProductComponents = subProductComponents;
	}

}
