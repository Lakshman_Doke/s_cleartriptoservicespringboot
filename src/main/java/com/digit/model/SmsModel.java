package com.digit.model;
/**
 * 
 */
import java.util.HashMap;

import lombok.Data;

@Data
public class SmsModel {
	String to;
	HashMap<String, String> parameters;
	String templateNo;
}
