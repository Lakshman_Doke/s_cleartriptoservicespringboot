package com.digit.model;

import java.sql.Timestamp;
/**
 * 
 * @author Lakshman.Doke
 *
 */
public class PersonCommunication {
	private long personCommunicationId;
	long personId;
	private long communicationType;
	private long subType;
	private String communicationValue;
	private boolean active;
	Timestamp createdDate;
	private long createdUserId;
	Timestamp lastModifiedDate;
	private long lastModifiedUserId;
	private Person person;
	private User lastModifiedUser;
	private User createdUser;

	public long getPersonCommunicationId() {
		return personCommunicationId;
	}

	public void setPersonCommunicationId(long personCommunicationId) {
		this.personCommunicationId = personCommunicationId;
	}

	public long getPersonId() {
		return personId;
	}

	public void setPersonId(long personId) {
		this.personId = personId;
	}

	public long getCommunicationType() {
		return communicationType;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public void setCommunicationType(long communicationType) {
		this.communicationType = communicationType;
	}

	public long getSubType() {
		return subType;
	}

	public void setSubType(long subType) {
		this.subType = subType;
	}

	public String getCommunicationValue() {
		return communicationValue;
	}

	public void setCommunicationValue(String communicationValue) {
		this.communicationValue = communicationValue;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public long getCreatedUserId() {
		return createdUserId;
	}

	public void setCreatedUserId(long createdUserId) {
		this.createdUserId = createdUserId;
	}

	public Timestamp getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Timestamp lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public long getLastModifiedUserId() {
		return lastModifiedUserId;
	}

	public void setLastModifiedUserId(long lastModifiedUserId) {
		this.lastModifiedUserId = lastModifiedUserId;
	}

	public User getLastModifiedUser() {
		return lastModifiedUser;
	}

	public void setLastModifiedUser(User lastModifiedUser) {
		this.lastModifiedUser = lastModifiedUser;
	}

	public User getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(User createdUser) {
		this.createdUser = createdUser;
	}
}
