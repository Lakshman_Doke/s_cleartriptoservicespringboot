package com.digit.model;
/**
 * 
 * @author Lakshman.Doke
 *
 */
public class ServiceResponse {
	String scheduledLink;

	String receiptLink;

	public String getScheduledLink() {
		return scheduledLink;
	}

	public void setScheduledLink(String scheduledLink) {
		this.scheduledLink = scheduledLink;
	}

	public String getReceiptLink() {
		return receiptLink;
	}

	public void setReceiptLink(String receiptLink) {
		this.receiptLink = receiptLink;
	}
}
