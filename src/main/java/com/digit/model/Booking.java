package com.digit.model;

import com.digit.entities.BookingData;
import com.google.gson.annotations.Expose;
/**
 * 
 * @author Lakshman.Doke
 *
 */
public class Booking {
	@Expose
	BookingData Booking;
	Integer ammendmentFlag;
	
	public Integer getAmmendmentFlag() {
		return ammendmentFlag;
	}

	public void setAmmendmentFlag(Integer ammendmentFlag) {
		this.ammendmentFlag = ammendmentFlag;
	}

	public BookingData getBooking() {
		return Booking;
	}

	public void setBooking(BookingData booking) {
		Booking = booking;
	}
}
