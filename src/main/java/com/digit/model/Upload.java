package com.digit.model;

import java.io.InputStream;
/**
 * 
 * @author Lakshman.Doke
 *
 */
public class Upload {
	InputStream inputstream;
	String fileName;

	public InputStream getInputstream() {
		return inputstream;
	}

	public void setInputstream(InputStream inputstream) {
		this.inputstream = inputstream;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
}
