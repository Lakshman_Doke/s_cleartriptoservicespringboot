package com.digit.model;

import java.io.Serializable;
import java.sql.Timestamp;

import lombok.Data;
/**
 * 
 * @author Lakshman.Doke
 *
 */
@Data
public class ApiKeyAdditionalDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long apiKeyAdditionalDetailsId;

	private String apiKey;

	private Timestamp craetedDate;

	private String key;

	private Timestamp lastModifiedDate;

	private Long productCode;

	private String value;

	private Long userId;
}
