package com.digit.model;

import java.util.HashMap;
import java.util.List;
/**
 * 
 * @author Lakshman.Doke
 *
 */
public class Mail {
	private String from;
	private List<String> to;
	private List<String> cc;
	private List<String> bcc;
	private String subject;
	private String body;
	private List<String> attachment;
	private String templateNo;
	private HashMap<String, String> parameters;
	private HashMap<String, String> subjectParameters;

	public String getTemplateNo() {
		return templateNo;
	}

	public void setTemplateNo(String templateNo) {
		this.templateNo = templateNo;
	}

	public HashMap<String, String> getParameters() {
		return parameters;
	}

	public void setParameters(HashMap<String, String> parameters) {
		this.parameters = parameters;
	}

	public void setAttachment(List<String> attachment) {
		this.attachment = attachment;
	}

	public List<String> getAttachment() {
		return attachment;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public List<String> getTo() {
		return to;
	}

	public void setTo(List<String> to) {
		this.to = to;
	}

	public List<String> getCc() {
		return cc;
	}

	public void setCc(List<String> cc) {
		this.cc = cc;
	}

	public List<String> getBcc() {
		return bcc;
	}

	public void setBcc(List<String> bcc) {
		this.bcc = bcc;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public HashMap<String, String> getSubjectParameters() {
		return subjectParameters;
	}

	public void setSubjectParameters(HashMap<String, String> subjectParameters) {
		this.subjectParameters = subjectParameters;
	}

}
