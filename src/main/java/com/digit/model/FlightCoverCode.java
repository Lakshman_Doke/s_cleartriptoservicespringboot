package com.digit.model;

import lombok.Data;
/**
 * 
 * @author Lakshman.Doke
 *
 */
@Data
public class FlightCoverCode {
    private Long threshouldId;

    private String airlineCode;

    private String departure;

    private String threshold;

    private String coverCode;
}
