package com.digit.model;

import java.sql.Timestamp;

import lombok.Data;
/**
 * 
 * @author Lakshman.Doke
 *
 */
@Data
public class UserRole {
	private long user_role_id;
	private long userId;
	private long role_id;
	private Boolean active;
	private Timestamp createdDate;
	private long createdUserId;
	private Timestamp lastModifiedDate;
	private long lastModifiedUserId;
	private User user;
	private Role role;
	private User createdUser;
	private User lastModifiedUser;
}
