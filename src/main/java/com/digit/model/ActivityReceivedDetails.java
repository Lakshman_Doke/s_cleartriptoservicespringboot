package com.digit.model;

import java.sql.Timestamp;

/**
 * 
 * @author Lakshman.Doke
 *
 */
public class ActivityReceivedDetails {
	long actRecdDetailsId;
	// long activityId;
	String key;
	String value;
	Timestamp createdDate;
	long createdUserId;
	Timestamp lastModifiedDate;
	long lastModifiedUserId;
	private Activity activity;

	private User lastModifiedUser;

	private User createdUser;

	public Activity getActivity() {
		return activity;
	}

	public void setActivity(Activity activity) {
		this.activity = activity;
	}

	public long getActRecdDetailsId() {
		return actRecdDetailsId;
	}

	public void setActRecdDetailsId(long actRecdDetailsId) {
		this.actRecdDetailsId = actRecdDetailsId;
	}

//	public long getActivityId() {
//		return activityId;
//	}
//
//	public void setActivityId(long activityId) {
//		this.activityId = activityId;
//	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public long getCreatedUserId() {
		return createdUserId;
	}

	public void setCreatedUserId(long createdUserId) {
		this.createdUserId = createdUserId;
	}

	public Timestamp getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Timestamp lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public long getLastModifiedUserId() {
		return lastModifiedUserId;
	}

	public void setLastModifiedUserId(long lastModifiedUserId) {
		this.lastModifiedUserId = lastModifiedUserId;
	}

	public User getLastModifiedUser() {
		return lastModifiedUser;
	}

	public void setLastModifiedUser(User lastModifiedUser) {
		this.lastModifiedUser = lastModifiedUser;
	}

	public User getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(User createdUser) {
		this.createdUser = createdUser;
	}
}
