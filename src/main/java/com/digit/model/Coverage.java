package com.digit.model;

import java.util.List;

public class Coverage {
	private String classProductCode;
	private List<ProductComponent> productComponents = null;
	private String loadingPerc;
	private String coverCode;
	private String coverType;
	private String coverStartDate;
	private String coverExpiryDate;
	private String sumInsuredAmount;
	private Boolean selection;

	public String getClassProductCode() {
		return classProductCode;
	}

	public void setClassProductCode(String classProductCode) {
		this.classProductCode = classProductCode;
	}

	public List<ProductComponent> getProductComponents() {
		return productComponents;
	}

	public void setProductComponents(List<ProductComponent> productComponents) {
		this.productComponents = productComponents;
	}

	public String getLoadingPerc() {
		return loadingPerc;
	}

	public void setLoadingPerc(String loadingPerc) {
		this.loadingPerc = loadingPerc;
	}

	public String getCoverCode() {
		return coverCode;
	}

	public void setCoverCode(String coverCode) {
		this.coverCode = coverCode;
	}

	public String getCoverType() {
		return coverType;
	}

	public void setCoverType(String coverType) {
		this.coverType = coverType;
	}

	public String getCoverStartDate() {
		return coverStartDate;
	}

	public void setCoverStartDate(String coverStartDate) {
		this.coverStartDate = coverStartDate;
	}

	public String getCoverExpiryDate() {
		return coverExpiryDate;
	}

	public void setCoverExpiryDate(String coverExpiryDate) {
		this.coverExpiryDate = coverExpiryDate;
	}

	public String getSumInsuredAmount() {
		return sumInsuredAmount;
	}

	public void setSumInsuredAmount(String sumInsuredAmount) {
		this.sumInsuredAmount = sumInsuredAmount;
	}

	public Boolean getSelection() {
		return selection;
	}

	public void setSelection(Boolean selection) {
		this.selection = selection;
	}
}
