package com.digit.model;
/**
 * 
 */
import java.util.List;

public class SubProductComponent {
	private String productComponentNumber;
	private String sumAmount;
	private transient String coverageMapping;
	private List<SubProductComponent> subProductComponents;

	public String getProductComponentNumber() {
		return productComponentNumber;
	}

	public void setProductComponentNumber(String productComponentNumber) {
		this.productComponentNumber = productComponentNumber;
	}

	public String getSumAmount() {
		return sumAmount;
	}

	public void setSumAmount(String sumAmount) {
		this.sumAmount = sumAmount;
	}

	public String getCoverageMapping() {
		return coverageMapping;
	}

	public void setCoverageMapping(String coverageMapping) {
		this.coverageMapping = coverageMapping;
	}

	public List<SubProductComponent> getSubProductComponents() {
		return subProductComponents;
	}

	public void setSubProductComponents(List<SubProductComponent> subProductComponents) {
		this.subProductComponents = subProductComponents;
	}

}
