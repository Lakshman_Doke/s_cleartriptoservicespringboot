package com.digit.model;

import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * 
 * @author Lakshman.Doke
 *
 */
@Data
@NoArgsConstructor
public class UserCredentials {
	private String login;
	private String password;
	private Long userId;
	private Long appId;
	private String apiKey;

	public UserCredentials(String login, String password) {
		this.login = login;
		this.password = password;
	}

	public UserCredentials(Long userId, String apiKey) {
		this.userId = userId;
		this.apiKey = apiKey;
	}
}
