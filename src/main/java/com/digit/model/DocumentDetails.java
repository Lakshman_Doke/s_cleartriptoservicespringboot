package com.digit.model;

import java.sql.Timestamp;
/**
 * 
 * @author Lakshman.Doke
 *
 */
public class DocumentDetails {
	private long documentId;
	private long activityId;
	private String documentName;
	private int documentType;
	private String s3Link;
	private String ABSDocumentKey;
	private Timestamp createdDate;
	private long createdUserId;
	private Timestamp lastModifiedDate;
	private long lastModifiedUserId;

	private Activity activity;

	private User createdUser;
	private User lastModifiedUser;

	public long getDocumentId() {
		return documentId;
	}

	public void setDocumentId(long documentId) {
		this.documentId = documentId;
	}

	public long getActivityId() {
		return activityId;
	}

	public void setActivityId(long activityId) {
		this.activityId = activityId;
	}

	public String getDocumentName() {
		return documentName;
	}

	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	public int getDocumentType() {
		return documentType;
	}

	public void setDocumentType(int documentType) {
		this.documentType = documentType;
	}

	public String getS3Link() {
		return s3Link;
	}

	public void setS3Link(String s3Link) {
		this.s3Link = s3Link;
	}

	public String getABSDocumentKey() {
		return ABSDocumentKey;
	}

	public void setABSDocumentKey(String aBSDocumentKey) {
		ABSDocumentKey = aBSDocumentKey;
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public long getCreatedUserId() {
		return createdUserId;
	}

	public void setCreatedUserId(long createdUserId) {
		this.createdUserId = createdUserId;
	}

	public Timestamp getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Timestamp lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public long getLastModifiedUserId() {
		return lastModifiedUserId;
	}

	public void setLastModifiedUserId(long lastModifiedUserId) {
		this.lastModifiedUserId = lastModifiedUserId;
	}

	public Activity getActivity() {
		return activity;
	}

	public void setActivity(Activity activity) {
		this.activity = activity;
	}

	public User getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(User createdUser) {
		this.createdUser = createdUser;
	}

	public User getLastModifiedUser() {
		return lastModifiedUser;
	}

	public void setLastModifiedUser(User lastModifiedUser) {
		this.lastModifiedUser = lastModifiedUser;
	}
}
