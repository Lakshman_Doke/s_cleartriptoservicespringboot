package com.digit.model;

import java.sql.Timestamp;
import java.util.List;
/**
 * 
 * @author Lakshman.Doke
 *
 */
public class User {
	private long userId;
	private String userName;
	private long type;
	private String login;
	private String password;
	private String apiKey;
	private boolean active;
	private Timestamp createdDate;
	private long createdUserId;
	private Timestamp lastModifiedDate;
	private long lastModifiedUserId;
	private List<Activity> LastModifiedActivities;
	private List<Activity> CreatedActivities;
	private List<ActivityAdditionalDetails> LastModifiedActivityAdditonalDetail;
	private List<ActivityAdditionalDetails> CreatedActivitiesAdditinalDetails;
	private List<ActivityReceivedDetails> LastModifiedReceivedActivities;
	private List<ActivityReceivedDetails> CreatedReceivedActivities;
	private List<ActivityStatusChange> LastModifiedStatusActivities;
	private List<ActivityStatusChange> CreatedStatusActivities;
	private List<Person> LastModifiedPersons;
	private List<Person> CreatedPerons;
	private List<PersonCommunication> LastModifiedPersonCommunications;
	private List<PersonCommunication> CreatedPersonCommunications;
	private List<User> LastModifiedUserList;
	private List<User> CreatedUserList;
	private List<UserRole> UserRole;
	private User lastModifiedUser;
	private User createdUser;
	private List<UserRole> LastModifiedUserRoles;
	private List<UserRole> createdUserRole;
	private List<DocumentDetails> LastModifiedDocumentDetails;
	private List<DocumentDetails> CreatedDocumentDetails;

	public List<UserRole> getUserRole() {
		return UserRole;
	}

	public void setUserRole(List<UserRole> userRole) {
		UserRole = userRole;
	}

	public List<UserRole> getLastModifiedUserRoles() {
		return LastModifiedUserRoles;
	}

	public void setLastModifiedUserRoles(List<UserRole> lastModifiedUserRoles) {
		LastModifiedUserRoles = lastModifiedUserRoles;
	}

	public List<UserRole> getCreatedUserRole() {
		return createdUserRole;
	}

	public void setCreatedUserRole(List<UserRole> createdUserRole) {
		this.createdUserRole = createdUserRole;
	}

	public List<DocumentDetails> getLastModifiedDocumentDetails() {
		return LastModifiedDocumentDetails;
	}

	public void setLastModifiedDocumentDetails(List<DocumentDetails> lastModifiedDocumentDetails) {
		LastModifiedDocumentDetails = lastModifiedDocumentDetails;
	}

	public List<DocumentDetails> getCreatedDocumentDetails() {
		return CreatedDocumentDetails;
	}

	public void setCreatedDocumentDetails(List<DocumentDetails> createdDocumentDetails) {
		CreatedDocumentDetails = createdDocumentDetails;
	}

	public List<User> getLastModifiedUserList() {
		return LastModifiedUserList;
	}

	public void setLastModifiedUserList(List<User> lastModifiedUserList) {
		LastModifiedUserList = lastModifiedUserList;
	}

	public List<User> getCreatedUserList() {
		return CreatedUserList;
	}

	public void setCreatedUserList(List<User> createdUserList) {
		CreatedUserList = createdUserList;
	}

	public User getLastModifiedUser() {
		return lastModifiedUser;
	}

	public void setLastModifiedUser(User lastModifiedUser) {
		this.lastModifiedUser = lastModifiedUser;
	}

	public User getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(User createdUser) {
		this.createdUser = createdUser;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public List<Activity> getLastModifiedActivities() {
		return LastModifiedActivities;
	}

	public void setLastModifiedActivities(List<Activity> lastModifiedActivities) {
		LastModifiedActivities = lastModifiedActivities;
	}

	public List<Activity> getCreatedActivities() {
		return CreatedActivities;
	}

	public void setCreatedActivities(List<Activity> createdActivities) {
		CreatedActivities = createdActivities;
	}

	public List<ActivityAdditionalDetails> getLastModifiedActivityAdditonalDetail() {
		return LastModifiedActivityAdditonalDetail;
	}

	public void setLastModifiedActivityAdditonalDetail(
			List<ActivityAdditionalDetails> lastModifiedActivityAdditonalDetail) {
		LastModifiedActivityAdditonalDetail = lastModifiedActivityAdditonalDetail;
	}

	public List<ActivityAdditionalDetails> getCreatedActivitiesAdditinalDetails() {
		return CreatedActivitiesAdditinalDetails;
	}

	public void setCreatedActivitiesAdditinalDetails(
			List<ActivityAdditionalDetails> createdActivitiesAdditinalDetails) {
		CreatedActivitiesAdditinalDetails = createdActivitiesAdditinalDetails;
	}

	public List<ActivityReceivedDetails> getLastModifiedReceivedActivities() {
		return LastModifiedReceivedActivities;
	}

	public void setLastModifiedReceivedActivities(List<ActivityReceivedDetails> lastModifiedReceivedActivities) {
		LastModifiedReceivedActivities = lastModifiedReceivedActivities;
	}

	public List<ActivityReceivedDetails> getCreatedReceivedActivities() {
		return CreatedReceivedActivities;
	}

	public void setCreatedReceivedActivities(List<ActivityReceivedDetails> createdReceivedActivities) {
		CreatedReceivedActivities = createdReceivedActivities;
	}

	public List<ActivityStatusChange> getLastModifiedStatusActivities() {
		return LastModifiedStatusActivities;
	}

	public void setLastModifiedStatusActivities(List<ActivityStatusChange> lastModifiedStatusActivities) {
		LastModifiedStatusActivities = lastModifiedStatusActivities;
	}

	public List<ActivityStatusChange> getCreatedStatusActivities() {
		return CreatedStatusActivities;
	}

	public void setCreatedStatusActivities(List<ActivityStatusChange> createdStatusActivities) {
		CreatedStatusActivities = createdStatusActivities;
	}

	public List<Person> getLastModifiedPersons() {
		return LastModifiedPersons;
	}

	public void setLastModifiedPersons(List<Person> lastModifiedPersons) {
		LastModifiedPersons = lastModifiedPersons;
	}

	public List<Person> getCreatedPerons() {
		return CreatedPerons;
	}

	public void setCreatedPerons(List<Person> createdPerons) {
		CreatedPerons = createdPerons;
	}

	public List<PersonCommunication> getLastModifiedPersonCommunications() {
		return LastModifiedPersonCommunications;
	}

	public void setLastModifiedPersonCommunications(List<PersonCommunication> lastModifiedPersonCommunications) {
		LastModifiedPersonCommunications = lastModifiedPersonCommunications;
	}

	public List<PersonCommunication> getCreatedPersonCommunications() {
		return CreatedPersonCommunications;
	}

	public void setCreatedPersonCommunications(List<PersonCommunication> createdPersonCommunications) {
		CreatedPersonCommunications = createdPersonCommunications;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public long getType() {
		return type;
	}

	public void setType(long type) {
		this.type = type;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public long getCreatedUserId() {
		return createdUserId;
	}

	public void setCreatedUserId(long createdUserId) {
		this.createdUserId = createdUserId;
	}

	public Timestamp getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Timestamp lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public Long getLastModifiedUserId() {
		return lastModifiedUserId;
	}

	public void setLastModifiedUserId(long lastModifiedUserId) {
		this.lastModifiedUserId = lastModifiedUserId;
	}
}
