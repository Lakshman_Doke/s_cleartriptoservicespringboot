package com.digit.model;

/**
 * 
 * @author Lakshman.Doke
 *
 */
public class Address {
	private static final Address instance = new Address();
	private String addressType;
	private String houseNumber;
	private String street;
	private String area;
	private String city;
	private String state;
	private String country;
	private String pin;

	public static Address getInstance() {
		return instance;
	}

	public String getAddressType() {
		return addressType;
	}

	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}

	public String getHouseNumber() {
		return houseNumber;
	}

	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public static Address createdummyAddress() {
		var address = new Address();
		address.setAddressType("R");
		address.setArea("Kormangala 5th Block");
		address.setCity("Bangalore");
		address.setCountry("IN");
		address.setPin("560034");
		address.setHouseNumber("94-95");
		address.setStreet("4th B Cross Road");
		return address;
	}
}
