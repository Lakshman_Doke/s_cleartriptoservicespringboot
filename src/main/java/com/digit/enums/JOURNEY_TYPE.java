package com.digit.enums;

import java.util.HashMap;
import java.util.Map;
/**
 * 
 * @author Lakshman.Doke
 *
 */
public enum JOURNEY_TYPE {

    Onward("O"), Return("R");

    static final Map<String, JOURNEY_TYPE> errorMap = new HashMap<>();

    private final String text;

    private JOURNEY_TYPE(final String text) {
        this.text = text;
    }

    public String toString() {
        return text;
    }

    public static JOURNEY_TYPE getErrorCode(String str) {
        return errorMap.get(str);
    }

    static {
        for (JOURNEY_TYPE c : JOURNEY_TYPE.values())
            errorMap.put(c.toString(), c);
    }
}
