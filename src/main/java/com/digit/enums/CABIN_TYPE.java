package com.digit.enums;

import java.util.HashMap;
import java.util.Map;

public enum CABIN_TYPE {
    Economy("E"), Business("B"), Gold("G"), Silver("S");

    static final Map<String, CABIN_TYPE> errorMap = new HashMap<>();

    private final String text;

    private CABIN_TYPE(final String text) {
        this.text = text;
    }

    public String toString() {
        return text;
    }

    public static CABIN_TYPE getErrorCode(String str) {
        return errorMap.get(str);
    }

    static {
        for (CABIN_TYPE c : CABIN_TYPE.values())
            errorMap.put(c.toString(), c);
    }
}
