package com.digit.enums;

import java.util.HashMap;
import java.util.Map;
/**
 * 
 * @author Lakshman.Doke
 *
 */
public enum GENDER {
    Male("ML"), male("ML"), Female("FE"), female("FE"), FEMALE("FE"), MALE("ML"), UNKOWN("UN"), UNKNOWN("UN"), unknown(
            "UN"), Unknown("UN");

    static final Map<String, GENDER> errorMap = new HashMap<>();

    static {
        for (GENDER c : GENDER.values())
            errorMap.put(c.toString(), c);
    }

    private final String text;

    GENDER(final String text) {
        this.text = text;
    }

    public static GENDER getErrorCode(String str) {
        return errorMap.get(str);
    }

    public String toString() {
        return text;
    }
}
