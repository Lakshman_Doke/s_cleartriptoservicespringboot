package com.digit.exception;

/**
 * 
 * @author Lakshman.Doke
 *
 */
public class PolicyCreationException extends Exception {
	private static final long serialVersionUID = 1L;

	public PolicyCreationException() {
		super();
	}

	public PolicyCreationException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public PolicyCreationException(String message, Throwable cause) {
		super(message, cause);
	}

	public PolicyCreationException(String message) {
		super(message);
	}

	public PolicyCreationException(Throwable cause) {
		super(cause);
	}
}
