package com.digit.exception;

/**
 * 
 * @author Lakshman.Doke
 *
 */
public class ActivityException extends Exception {
	private static final long serialVersionUID = 1L;

	public ActivityException() {
		super();
	}

	public ActivityException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public ActivityException(String message, Throwable cause) {
		super(message, cause);
	}

	public ActivityException(String message) {
		super(message);
	}

	public ActivityException(Throwable cause) {
		super(cause);
	}
}
