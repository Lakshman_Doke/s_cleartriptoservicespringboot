package com.digit.exception;

/**
 * 
 * @author Lakshman.Doke
 *
 */
public class UrlShorterException extends Exception {
	private static final long serialVersionUID = 1L;

	public UrlShorterException() {
		super();
	}

	public UrlShorterException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public UrlShorterException(String message, Throwable cause) {
		super(message, cause);
	}

	public UrlShorterException(String message) {
		super(message);
	}

	public UrlShorterException(Throwable cause) {
		super(cause);
	}
}
