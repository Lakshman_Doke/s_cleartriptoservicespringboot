package com.digit.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.google.gson.annotations.Expose;
/**
 * 
 * @author Lakshman.Doke
 *
 */
@Entity
@Table(name = "t_payment", schema = "digit_travel")
public class Payment {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "payment_id", nullable = false)
	Long id;

	@Column(name = "paymenttype")
	@Expose
	String PaymentType;

	@Column(name = "paymentamount")
	@Expose
	String PaymentAmount;

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, targetEntity = BookingData.class)
	@JoinColumn(name = "bookingdataid", referencedColumnName = "bookingDataId")
	BookingData bookingData;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BookingData getBookingData() {
		return bookingData;
	}

	public void setBookingData(BookingData bookingData) {
		this.bookingData = bookingData;
	}

	public String getPaymentType() {
		return PaymentType;
	}

	public void setPaymentType(String paymentType) {
		PaymentType = paymentType;
	}

	public String getPaymentAmount() {
		return PaymentAmount;
	}

	public void setPaymentAmount(String paymentAmount) {
		PaymentAmount = paymentAmount;
	}
}
