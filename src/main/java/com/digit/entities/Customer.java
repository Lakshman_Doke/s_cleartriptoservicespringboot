package com.digit.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.google.gson.annotations.Expose;
/**
 * 
 * @author Lakshman.Doke
 *
 */
@Entity
@Table(name = "t_customer", schema = "digit_travel")
public class Customer {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "customer_id", nullable = false)
	Long id;

	@Column(name = "title")
	@Expose
	String Title;

	@Column(name = "firstname")
	@Expose
	String FirstName;

	@Column(name = "lastname")
	@Expose
	String LastName;

	@Column(name = "address1")
	@Expose
	String Address1;

	@Column(name = "address2")
	@Expose
	String Address2;

	@Column(name = "city")
	@Expose
	String City;

	@Column(name = "pincode")
	@Expose
	String PinCode;

	@Column(name = "state")
	@Expose
	String State;

	@Column(name = "country")
	@Expose
	String Country;

	@Column(name = "gender")
	@Expose
	String Gender;

	@Column(name = "dob")
	@Expose
	String DOB;

	@Column(name = "mobile")
	@Expose
	String Mobile;

	@Column(name = "email")
	@Expose
	String Email;



	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, targetEntity = BookingData.class)
	@JoinColumn(name = "bookingdataid", referencedColumnName = "bookingDataId")
	BookingData bookingData;

	@Transient
	@Expose
	private String companyName;

	String CustomerGSTIN;

	public BookingData getBookingData() {
		return bookingData;
	}

	public void setBookingData(BookingData bookingData) {
		this.bookingData = bookingData;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return Title;
	}

	public void setTitle(String title) {
		Title = title;
	}

	public String getFirstName() {
		return FirstName;
	}

	public void setFirstName(String firstName) {
		FirstName = firstName;
	}

	public String getLastName() {
		return LastName;
	}

	public void setLastName(String lastName) {
		LastName = lastName;
	}

	public String getAddress1() {
		return Address1;
	}

	public void setAddress1(String address1) {
		Address1 = address1;
	}

	public String getAddress2() {
		return Address2;
	}

	public void setAddress2(String address2) {
		Address2 = address2;
	}

	public String getCity() {
		return City;
	}

	public void setCity(String city) {
		City = city;
	}

	public String getPinCode() {
		return PinCode;
	}

	public void setPinCode(String pinCode) {
		PinCode = pinCode;
	}

	public String getState() {
		return State;
	}

	public void setState(String state) {
		State = state;
	}

	public String getCountry() {
		return Country;
	}

	public void setCountry(String country) {
		Country = country;
	}

	public String getGender() {
		return Gender;
	}

	public void setGender(String gender) {
		Gender = gender;
	}

	public String getDOB() {
		return DOB;
	}

	public void setDOB(String dOB) {
		DOB = dOB;
	}

	public String getMobile() {
		return Mobile;
	}

	public void setMobile(String mobile) {
		Mobile = mobile;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public String getCustomerGSTIN() {
		return CustomerGSTIN;
	}

	public void setCustomerGSTIN(String customerGSTIN) {
		CustomerGSTIN = customerGSTIN;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

}
