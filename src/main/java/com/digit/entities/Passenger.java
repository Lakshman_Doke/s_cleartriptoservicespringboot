package com.digit.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Fetch;

import com.google.gson.annotations.Expose;
/**
 * 
 * @author Lakshman.Doke
 *
 */
@Entity
@Table(name = "t_passenger", schema = "digit_travel")
public class Passenger {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "passenger_id", nullable = false)
	Long id;

	@Column(name = "passengercode")
	@Expose
	String PassengerCode;

	@Column(name = "title")
	@Expose
	String Title;

	@Column(name = "firstname")
	@Expose
	String FirstName;

	@Column(name = "lastname")
	@Expose
	String LastName;

	@Column(name = "gender")
	@Expose
	String Gender;

	@Column(name = "dob")
	@Expose
	String DOB;

	@Column(name = "address1")
	@Expose
	String Address1;

	@Column(name = "address2")
	@Expose
	String Address2;

	@Column(name = "mobile")
	@Expose
	String Mobile;

	@Column(name = "email")
	@Expose
	String Email;

	@Column(name = "insurancepremium")
	@Expose
	String InsurancePremium;

	@Column(name = "policynumber")
	String policyNumber;

	@Expose
	@Column(name = "paxid")
	String paxId;

	@Column(name = "total_fare")
	@Expose
	Double totalFare;

	@Column(name = "is_onwards_claimed")
	Boolean isOnwardsClaimed;

	@Column(name = "is_return_claimed")
	Boolean isReturnClaimed;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "passenger")
	@Fetch(org.hibernate.annotations.FetchMode.SUBSELECT)
	@Expose
	List<Flight> Flights;

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, targetEntity = BookingData.class)
	@JoinColumn(name = "bookingdataid", referencedColumnName = "bookingDataId", nullable = false, insertable = true, updatable = true)
	BookingData bookingData;

	@Transient
	String status;

	public BookingData getBookingData() {
		return bookingData;
	}

	public void setBookingData(BookingData bookingData) {
		this.bookingData = bookingData;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPassengerCode() {
		return PassengerCode;
	}

	public void setPassengerCode(String passengerCode) {
		PassengerCode = passengerCode;
	}

	public String getTitle() {
		return Title;
	}

	public void setTitle(String title) {
		Title = title;
	}

	public String getFirstName() {
		return FirstName;
	}

	public void setFirstName(String firstName) {
		FirstName = firstName;
	}

	public String getLastName() {
		return LastName;
	}

	public void setLastName(String lastName) {
		LastName = lastName;
	}

	public String getGender() {
		return Gender;
	}

	public void setGender(String gender) {
		Gender = gender;
	}

	public String getDOB() {
		return DOB;
	}

	public void setDOB(String dOB) {
		DOB = dOB;
	}

	public String getAddress1() {
		return Address1;
	}

	public void setAddress1(String address1) {
		Address1 = address1;
	}

	public String getAddress2() {
		return Address2;
	}

	public void setAddress2(String address2) {
		Address2 = address2;
	}

	public String getMobile() {
		return Mobile;
	}

	public void setMobile(String mobile) {
		Mobile = mobile;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public List<Flight> getFlights() {
		return Flights;
	}

	public void setFlights(List<Flight> flights) {
		Flights = flights;
	}

	public String getInsurancePremium() {
		return InsurancePremium;
	}

	public void setInsurancePremium(String insurancePremium) {
		InsurancePremium = insurancePremium;
	}

	public String getPolicyNumber() {
		return policyNumber;
	}

	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}

	public String getPaxId() {
		return paxId;
	}

	public void setPaxId(String paxId) {
		this.paxId = paxId;
	}

	public Double getTotalFare() {
		return totalFare;
	}

	public void setTotalFare(Double totalFare) {
		this.totalFare = totalFare;
	}

	public Boolean getIsOnwardsClaimed() {
		return isOnwardsClaimed;
	}

	public void setIsOnwardsClaimed(Boolean isOnwardsClaimed) {
		this.isOnwardsClaimed = isOnwardsClaimed;
	}

	public Boolean getIsReturnClaimed() {
		return isReturnClaimed;
	}

	public void setIsReturnClaimed(Boolean isReturnClaimed) {
		this.isReturnClaimed = isReturnClaimed;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
