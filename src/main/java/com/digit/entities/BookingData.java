package com.digit.entities;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Fetch;

import com.google.gson.annotations.Expose;
/**
 * 
 * @author Lakshman.Doke
 *
 */
@Entity
@Table(name = "t_booking_data", schema = "digit_travel")
public class BookingData {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "bookingdataid")
	Long bookingDataId;
	
	@Column(name = "tripid")
	@Expose
	String TripId;
	
	@Expose
	@Column(name = "bookingdate")
	String BookingDate;
	
	@Expose
	@Column(name = "noofpassengers")
	String NoOfPassengers;
	
	@Column(name = "triptype")
	@Expose
	String TripType;
	
	@Column(name = "channel")
	@Expose
	String Channel;
	
	@Column(name = "partystate")
	@Expose
	String PartyState;
	
	@Column(name = "partygstin")
	@Expose
	String PartyGSTIN;

	@Expose
	@Column(name = "fare_rules")
	String fareRules;

	@Expose
	@Column(name = "sub_insurance_product_code")
	String subInsuranceProductCode;

	@Column
	Timestamp createdDate;

	@Column(name = "lastmodifieddate")
	Timestamp lastModifiedDate;

	@Expose
	@Transient
	String UpdateStatus;

	@Expose
	@Transient
	String DIGITPolicyNo;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "bookingData")
	@Expose
	Customer Customer;
	
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "bookingData")
	@Expose
	Payment Payment;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "bookingData")
	@Fetch(org.hibernate.annotations.FetchMode.SUBSELECT)
	@Expose
	List<Passenger> Passengers;

	@Transient
	int flag;

	@Column(name = "inretry")
	boolean inRetry;

	@Expose
	@Column(name = "quick_quote_id")
	String quoteId;

	@Transient
	@Expose
	String type;

	@Expose
	@Column(name = "is_claim_trigger_successful")
	Boolean isClaimTriggerSuccessful;
	
	public boolean isInRetry() {
		return inRetry;
	}

	public void setInRetry(boolean inRetry) {
		this.inRetry = inRetry;
	}

	public String getSubInsuranceProductCode() {
		return subInsuranceProductCode;
	}

	public void setSubInsuranceProductCode(String subInsuranceProductCode) {
		this.subInsuranceProductCode = subInsuranceProductCode;
	}

	public Long getBookingDataId() {
		return bookingDataId;
	}

	public void setBookingDataId(Long bookingDataId) {
		this.bookingDataId = bookingDataId;
	}

	public int getFlag() {
		return flag;
	}

	public void setFlag(int flag) {
		this.flag = flag;
	}

	public String getTripId() {
		return TripId;
	}

	public void setTripId(String tripId) {
		TripId = tripId;
	}

	public String getBookingDate() {
		return BookingDate;
	}

	public void setBookingDate(String bookingDate) {
		BookingDate = bookingDate;
	}

	public String getNoOfPassengers() {
		return NoOfPassengers;
	}

	public void setNoOfPassengers(String noOfPassengers) {
		NoOfPassengers = noOfPassengers;
	}

	public String getTripType() {
		return TripType;
	}

	public void setTripType(String tripType) {
		TripType = tripType;
	}

	public String getChannel() {
		return Channel;
	}

	public void setChannel(String channel) {
		Channel = channel;
	}

	public String getPartyState() {
		return PartyState;
	}

	public void setPartyState(String partyState) {
		PartyState = partyState;
	}

	public String getPartyGSTIN() {
		return PartyGSTIN;
	}

	public void setPartyGSTIN(String partyGSTIN) {
		PartyGSTIN = partyGSTIN;
	}

	public Customer getCustomer() {
		return Customer;
	}

	public void setCustomer(Customer customer) {
		Customer = customer;
	}

	public Payment getPayment() {
		return Payment;
	}

	public void setPayment(Payment payment) {
		Payment = payment;
	}

	public List<Passenger> getPassengers() {
		return Passengers;
	}

	public void setPassengers(List<Passenger> passengers) {
		Passengers = passengers;
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public Timestamp getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Timestamp lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public String getFareRules() {
		return fareRules;
	}

	public void setFareRules(String fareRules) {
		this.fareRules = fareRules;
	}

	public String getQuoteId() {
		return quoteId;
	}

	public void setQuoteId(String quoteId) {
		this.quoteId = quoteId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUpdateStatus() {
		return UpdateStatus;
	}

	public void setUpdateStatus(String updateStatus) {
		UpdateStatus = updateStatus;
	}

	public String getDIGITPolicyNo() {
		return DIGITPolicyNo;
	}

	public void setDIGITPolicyNo(String dIGITPolicyNo) {
		DIGITPolicyNo = dIGITPolicyNo;
	}

	public Boolean getIsClaimTriggerSuccessful() {
		return isClaimTriggerSuccessful;
	}

	public void setIsClaimTriggerSuccessful(Boolean isClaimTriggerSuccessful) {
		this.isClaimTriggerSuccessful = isClaimTriggerSuccessful;
	}

}
