package com.digit.entities;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
/**
 * 
 * @author Lakshman.Doke
 *
 */
@Entity
@Table(name = "t_travel_quick_quote", schema = "DIGIT_COMMON")
public class TravelQuickQuote {
	@EmbeddedId
	private TravelQuickQuoteId travelQuickQuoteId;

	@Column(name = "request_Time")
	private Timestamp requestTime;

	@Column(name = "package_Code")
	private String packageCode;

	@Column(name = "product_Sequence_Number")
	private Long productSequenceNumber;

	@Column(name = "imd_Code")
	private String imdCode;

	@Column(name = "random_Number")
	private BigDecimal randomNumber;

	private BigDecimal coefficient;

	@Column(name = "created_Date")
	private Timestamp createdDate;

	@Column(name = "travel_Date")
	private Date travelDate;

	public TravelQuickQuoteId getTravelQuickQuoteId() {
		return travelQuickQuoteId;
	}

	public void setTravelQuickQuoteId(TravelQuickQuoteId travelQuickQuoteId) {
		this.travelQuickQuoteId = travelQuickQuoteId;
	}

	public Timestamp getRequestTime() {
		return requestTime;
	}

	public void setRequestTime(Timestamp requestTime) {
		this.requestTime = requestTime;
	}

	public String getPackageCode() {
		return packageCode;
	}

	public void setPackageCode(String packageCode) {
		this.packageCode = packageCode;
	}

	public Long getProductSequenceNumber() {
		return productSequenceNumber;
	}

	public void setProductSequenceNumber(Long productSequenceNumber) {
		this.productSequenceNumber = productSequenceNumber;
	}

	public String getImdCode() {
		return imdCode;
	}

	public void setImdCode(String imdCode) {
		this.imdCode = imdCode;
	}

	public BigDecimal getRandomNumber() {
		return randomNumber;
	}

	public void setRandomNumber(BigDecimal randomNumber) {
		this.randomNumber = randomNumber;
	}

	public BigDecimal getCoefficient() {
		return coefficient;
	}

	public void setCoefficient(BigDecimal coefficient) {
		this.coefficient = coefficient;
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public Date getTravelDate() {
		return travelDate;
	}

	public void setTravelDate(Date travelDate) {
		this.travelDate = travelDate;
	}

}
