package com.digit.entities;

import lombok.Data;

import javax.persistence.*;
import java.sql.Timestamp;
/**
 * 
 * @author Lakshman.Doke
 *
 */
@Entity
@Table(name = "t_policy_document_config", schema = "digit_config")
@Data
public class PolicyDocumentConfig {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "policy_document_id")
    private Long policyDocumentId;

    @Column(name = "product_code")
    private String productCode;

    @Column(name = "amendment_code")
    private String amendmentCode;

    @Column(name = "product_offering_code")
    private String productOfferingCode;

    @Column(name = "mail_template")
    private String mailTemplate;

    @Column(name = "sms_template")
    private String smsTemplate;

    @Column(name = "created_date")
    private Timestamp createdDate;

    @Column(name = "last_modified_date")
    private Timestamp lastModifiedDate;

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "active")
    private Boolean active;

    @Column(name = "pdfgeneration_service")
    private Integer pdfGenerationService;

    @Column(name = "activity_type_id")
    private Integer activityTypeId;

    @Column(name = "wording")
    private String wording;

    @Column(name = "wording_flag")
    private Boolean wordingFlag;

    @Column(name = "summary")
    private String summary;

    @Column(name = "summary_flag")
    private Boolean summaryFlag;

    @Column(name = "proposal_sc_pdf")
    private Boolean proposalSCPdf;

    @Column(name = "proposal_hc_pdf")
    private Boolean proposalHCPdf;

    @Column(name = "schedule_sc_pdf")
    private Boolean scheduleSCPdf;

    @Column(name = "schedule_hc_pdf")
    private Boolean scheduleHCPdf;

    @Column(name = "reciept_pdf")
    private Boolean recieptPdf;

    @Column(name = "activity_product_code")
    private Integer activityProductCode;

    @Column(name = "support_mail")
    private String supportMail;

    @Column(name = "prefered_language")
    private String preferedLanguage;

    @Column(name = "e_card_pdf")
    private Boolean eCardPdf;

    @Column(name = "contractualliability_pdf")
    private Boolean contractualLiabilityPdf;

    @Column(name = "irda_effective_date")
    private String irdaEffectiveDate;

    @Column(name = "claims_certificate_flag")
    private Boolean claimsCertificateFlag;

    @Column(name = "claims_certificate_url")
    private String claimsCertificateUrl;

    @Column(name = "invoice_pdf")
    private Boolean invoicePdf;

    @Column(name = "to_email")
    private String toEmail;

    @Column(name = "policy_type")
    private String policyType;

    @Column(name = "communication_to_be_send")
    private Boolean communicationToBeSend;

    @Column(name = "pdf_source")
    private String pdfSource;
}
