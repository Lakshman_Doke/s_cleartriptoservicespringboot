package com.digit.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.google.gson.annotations.Expose;
/**
 * 
 * @author Lakshman.Doke
 *
 */
@Entity
@Table(name = "t_flight", schema = "DIGIT_TRAVEL")
public class Flight {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "flight_id", nullable = false)
	Long id;

	@Column(name = "updatetype")
	@Expose
	String UpdateType;

	@Column(name = "digitpolicyno")
	@Expose
	String DIGITPolicyNo;

	@Column(name = "updatestatus")
	@Expose
	String UpdateStatus;

	@Column(name = "journeytype")
	@Expose
	String JourneyType;

	@Column(name = "leg")
	@Expose
	String Leg;

	@Column(name = "origin")
	@Expose
	String Origin;

	@Column(name = "destination")
	@Expose
	String Destination;

	@Column(name = "departuredate")
	@Expose
	String DepartureDate;

	@Column(name = "arrivaldate")
	@Expose
	String ArrivalDate;

	@Column(name = "carriername")
	@Expose
	String CarrierName;

	@Column(name = "carrierno")
	@Expose
	String CarrierNo;

	@Column(name = "carrierflightno")
	@Expose
	String CarrierFlightNo;

	@Column(name = "cabintype")
	@Expose
	String CabinType;

	@Column(name = "airlinepnr")
	@Expose
	String AirlinePNR;

	@Column(name = "refundableflag")
	@Expose
	String RefundableFlag;

	@Column(name = "originalcarriername")
	@Expose
	String OriginalCarrierName;

	@Column(name = "originalcarrierno")
	@Expose
	String OriginalCarrierNo;

	@Column(name = "originalcarrierflightno")
	@Expose
	String OriginalCarrierFlightNo;

	@Column(name = "originalairlinepnr")
	@Expose
	String OriginalAirlinePNR;

	@Column(name = "product")
	@Expose
	String Product;

	@Column(name = "alcancellationamt")
	@Expose
	String ALCancellationAmt;

	@Column(name = "ctcancellationamt")
	@Expose
	String CTCancellationAmt;

	@Column(name = "emailtransaction")
	String emailTransaction;

	@Column(name = "smstransaction")
	String smsTransaction;

	@Expose
	@Column(name = "fare")
	Double fare;

	@Expose
	@Column(name = "claim_ammount")
	Double claimAmmount;

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, targetEntity = Passenger.class)
	@JoinColumn(name = "Passenger_Id", referencedColumnName = "Passenger_Id", nullable = false, insertable = true, updatable = true)
	Passenger passenger;

	@Transient
	@Expose
	Double loadFactor;

	@Transient
	@Expose
	Double newFare;

	@Transient
	@Expose
	Double oldFare;

	public String getEmailTransaction() {
		return emailTransaction;
	}

	public void setEmailTransaction(String emailTransaction) {
		this.emailTransaction = emailTransaction;
	}

	public String getSmsTransaction() {
		return smsTransaction;
	}

	public void setSmsTransaction(String smsTransaction) {
		this.smsTransaction = smsTransaction;
	}

	public String getUpdateType() {
		return UpdateType;
	}

	public void setUpdateType(String updateType) {
		UpdateType = updateType;
	}

	public String getDIGITPolicyNo() {
		return DIGITPolicyNo;
	}

	public void setDIGITPolicyNo(String dIGITPolicyNo) {
		DIGITPolicyNo = dIGITPolicyNo;
	}

	public String getUpdateStatus() {
		return UpdateStatus;
	}

	public void setUpdateStatus(String updateStatus) {
		UpdateStatus = updateStatus;
	}

	public String getJourneyType() {
		return JourneyType;
	}

	public void setJourneyType(String journeyType) {
		JourneyType = journeyType;
	}

	public String getLeg() {
		return Leg;
	}

	public void setLeg(String leg) {
		Leg = leg;
	}

	public String getOrigin() {
		return Origin;
	}

	public void setOrigin(String origin) {
		Origin = origin;
	}

	public String getDestination() {
		return Destination;
	}

	public void setDestination(String destination) {
		Destination = destination;
	}

	public String getDepartureDate() {
		return DepartureDate;
	}

	public void setDepartureDate(String departureDate) {
		DepartureDate = departureDate;
	}

	public String getArrivalDate() {
		return ArrivalDate;
	}

	public void setArrivalDate(String arrivalDate) {
		ArrivalDate = arrivalDate;
	}

	public String getCarrierName() {
		return CarrierName;
	}

	public void setCarrierName(String carrierName) {
		CarrierName = carrierName;
	}

	public String getCarrierNo() {
		return CarrierNo;
	}

	public void setCarrierNo(String carrierNo) {
		CarrierNo = carrierNo;
	}

	public String getCarrierFlightNo() {
		return CarrierFlightNo;
	}

	public void setCarrierFlightNo(String carrierFlightNo) {
		CarrierFlightNo = carrierFlightNo;
	}

	public String getCabinType() {
		return CabinType;
	}

	public void setCabinType(String cabinType) {
		CabinType = cabinType;
	}

	public String getAirlinePNR() {
		return AirlinePNR;
	}

	public void setAirlinePNR(String airlinePNR) {
		AirlinePNR = airlinePNR;
	}

	public String getRefundableFlag() {
		return RefundableFlag;
	}

	public void setRefundableFlag(String refundableFlag) {
		RefundableFlag = refundableFlag;
	}

	public String getOriginalCarrierName() {
		return OriginalCarrierName;
	}

	public void setOriginalCarrierName(String originalCarrierName) {
		OriginalCarrierName = originalCarrierName;
	}

	public String getOriginalCarrierNo() {
		return OriginalCarrierNo;
	}

	public void setOriginalCarrierNo(String originalCarrierNo) {
		OriginalCarrierNo = originalCarrierNo;
	}

	public String getOriginalCarrierFlightNo() {
		return OriginalCarrierFlightNo;
	}

	public void setOriginalCarrierFlightNo(String originalCarrierFlightNo) {
		OriginalCarrierFlightNo = originalCarrierFlightNo;
	}

	public String getOriginalAirlinePNR() {
		return OriginalAirlinePNR;
	}

	public void setOriginalAirlinePNR(String originalAirlinePNR) {
		OriginalAirlinePNR = originalAirlinePNR;
	}

	public String getProduct() {
		return Product;
	}

	public void setProduct(String product) {
		Product = product;
	}

	public String getALCancellationAmt() {
		return ALCancellationAmt;
	}

	public void setALCancellationAmt(String aLCancellationAmt) {
		ALCancellationAmt = aLCancellationAmt;
	}

	public String getCTCancellationAmt() {
		return CTCancellationAmt;
	}

	public void setCTCancellationAmt(String cTCancellationAmt) {
		CTCancellationAmt = cTCancellationAmt;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Passenger getPassenger() {
		return passenger;
	}

	public void setPassenger(Passenger passenger) {
		this.passenger = passenger;
	}

	public Double getFare() {
		return fare;
	}

	public void setFare(Double fare) {
		this.fare = fare;
	}

	public Double getClaimAmmount() {
		return claimAmmount;
	}

	public void setClaimAmmount(Double claimAmmount) {
		this.claimAmmount = claimAmmount;
	}

	public Double getLoadFactor() {
		return loadFactor;
	}

	public void setLoadFactor(Double loadFactor) {
		this.loadFactor = loadFactor;
	}

	public Double getNewFare() {
		return newFare;
	}

	public void setNewFare(Double newFare) {
		this.newFare = newFare;
	}

	public Double getOldFare() {
		return oldFare;
	}

	public void setOldFare(Double oldFare) {
		this.oldFare = oldFare;
	}
}
