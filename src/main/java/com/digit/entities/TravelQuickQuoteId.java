package com.digit.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * 
 * @author Lakshman.Doke
 *
 */
@Embeddable
public class TravelQuickQuoteId implements Serializable {

	private static final long serialVersionUID = 1L;
	@Column(name = "quick_Quote_Id")
	private String quickQuoteId;
	private String airline;

	@Override
	public boolean equals(Object object) {
		if (this == object)
			return true;
		else if (object == null)
			return false;
		else if (object instanceof TravelQuickQuoteId) {
			TravelQuickQuoteId travelQuickQuoteId = (TravelQuickQuoteId) object;
			if ((travelQuickQuoteId.quickQuoteId == null && quickQuoteId == null)
					|| (travelQuickQuoteId.getQuickQuoteId().equals(quickQuoteId)
							&& travelQuickQuoteId.getAirline().equals(airline)))
				return true;
		}
		return false;
	}

	public String getQuickQuoteId() {
		return quickQuoteId;
	}

	public void setQuickQuoteId(String quickQuoteId) {
		this.quickQuoteId = quickQuoteId;
	}

	public String getAirline() {
		return airline;
	}

	public void setAirline(String airline) {
		this.airline = airline;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((airline == null) ? 0 : airline.hashCode());
		result = prime * result + ((quickQuoteId == null) ? 0 : quickQuoteId.hashCode());
		return result;
	}
}
