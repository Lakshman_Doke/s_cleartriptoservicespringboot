package com.digit.utility;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.digit.dpsModels.Policy;
import com.digit.entities.PolicyDocumentConfig;
import com.digit.exception.PolicyCreationException;
import com.digit.model.Booking;
import com.digit.pdfgeneration.TravelPdfGeneration;
import com.digit.repositories.PolicyDocumentConfigRepository;

/**
 * 
 * @author Lakshman.Doke
 *
 */
@Component
public class Process {
	@Autowired
	private PolicyDocumentConfigRepository policyDocumentConfigRepository;
	@Autowired
	private BookingToDps bookingToDps;
	@Autowired
	DpsClient dpsClient;
	TravelPdfGeneration travelPdfGeneration = new TravelPdfGeneration();

	public Booking process(Booking booking, String authKey) throws PolicyCreationException {
		Policy dpsPolicy = bookingToDps.convertJson(booking);
		Policy returnPolicy = dpsClient.callDps(dpsPolicy, authKey);
		bookingToDps.convertPolicyToBooking(returnPolicy, booking);

		List<PolicyDocumentConfig> configResult = policyDocumentConfigRepository
				.findByActiveAndProductOfferingCodeAndPdfSourceNotAndAmendmentCode(true,
						booking.getBooking().getSubInsuranceProductCode(), "CCM", "G01");
		if (!configResult.isEmpty()
				&& booking.getBooking().getSubInsuranceProductCode().equals(Constants.CN_PACKAGE_NAME)) {
			ExecutorService executorPdf = Executors.newFixedThreadPool(1);
			Runnable pdfGenerator = new PdfGenerator(booking);
			executorPdf.execute(pdfGenerator);
			executorPdf.shutdown();
		}

		return booking;
	}
}
