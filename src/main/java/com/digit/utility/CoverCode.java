package com.digit.utility;

import java.lang.reflect.Type;
import java.sql.Date;
import java.sql.Time;
import java.util.Calendar;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import com.digit.entities.Flight;
import com.digit.exception.ActivityException;
import com.digit.model.Address;
import com.digit.model.AirportCode;
import com.digit.model.FlightCoverCode;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * 
 * @author Lakshman.Doke
 *
 */
@Component
public class CoverCode {
	private static final Logger logger = LoggerFactory.getLogger(CoverCode.class);
	@Autowired
	private  PropertyConfiguration propertyConfiguration;

	public  String getCoverCode(Flight flight) {
		try {
			var timeString = flight.getDepartureDate().substring(11, 19);
			var time = Time.valueOf(timeString);
			var fixedTime = Time.valueOf("12:00:00");
			String departureString = null;
			String airlineCode = flight.getCarrierNo();
			var dateString = flight.getDepartureDate().substring(0, 10);

			var travelDate = Date.valueOf(dateString);

			var cal = Calendar.getInstance();
			cal.setTime(travelDate);

			int month = cal.get(Calendar.MONTH);
			month += 1;

			if (time.before(fixedTime))
				departureString = "-12.00";
			else if (time.after(fixedTime))
				departureString = "%2B12.00";
			else
				departureString = "-12.00";

			var finalDepartureString = departureString;
			var finalMonth = month;
			ResponseEntity<List<FlightCoverCode>> responseEntity = WebClient
					.create(propertyConfiguration.getTravelDataThreshold()).get()
					.uri(uriBuilder -> uriBuilder.queryParam("airlineCode", airlineCode)
							.queryParam("departure", finalDepartureString).queryParam("month", finalMonth)
							.queryParam("partner", propertyConfiguration.getGeneratePartner()).build())
					.header(HttpHeaders.AUTHORIZATION, propertyConfiguration.getTravelServiceApiKey())
					.accept(MediaType.APPLICATION_JSON).retrieve().toEntityList(FlightCoverCode.class).block();
			if (responseEntity.getStatusCode() == HttpStatus.OK) {
				logger.info("getCoverCode(Flight flight): received JSON", responseEntity.getBody());
				List<FlightCoverCode> flightCoverCode = responseEntity.getBody();
				return flightCoverCode.get(0).getCoverCode();
			} else {
				throw new ActivityException("Cover Code service Failed");
			}
		} catch (Exception e) {
			logger.info("getCoverCode(Flight flight): Exception", e);
		}
		return null;
	}

	public  Address getAddressByAirportCode(String airportCode) {
		ResponseEntity<String> responseEntity = WebClient.create().get()
				.uri(propertyConfiguration.getTravelDataService(),
						uriBuilder -> uriBuilder.queryParam("IATA", airportCode).build())
				.accept(MediaType.APPLICATION_JSON)
				.header(HttpHeaders.AUTHORIZATION, propertyConfiguration.getTravelServiceApiKey()).retrieve()
				.toEntity(String.class).block();
		if (responseEntity.getStatusCode() != HttpStatus.OK) {
			logger.error("getAddressByAirportCode(String airportCode): Airport Code service Failed..");
		}
		logger.info("getAddressByAirportCode(String airportCode): received JSON:", responseEntity.getBody());
		String receivedJson=responseEntity.getBody();
		List<AirportCode> airportList = getModelFromJsonAirport(receivedJson);
		Address address = new Address();
		address.setCity(airportList.get(0).getCityName().replaceAll("[^a-zA-Z\\s.+]", ""));
		address.setState(airportList.get(0).getState());
		address.setPin(airportList.get(0).getPinCode());
		address.setCountry("IN");
		return address;
	}
	public static List<AirportCode> getModelFromJsonAirport(String json) {
		Gson objGson = new Gson();
		Type listType = new TypeToken<List<AirportCode>>() {
		}.getType();
		return objGson.fromJson(json, listType);
	}

}
