package com.digit.utility;

import java.lang.reflect.Type;
import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import com.digit.exception.ActivityException;
import com.digit.model.ApiKeyAdditionalDetail;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import reactor.core.publisher.Mono;
/**
 * 
 * @author Lakshman.Doke
 *
 */
@Component
public class FleetNumberUtilController {
	WebClient webClient = WebClient.builder().defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
			.build();

	@Autowired
	PropertyConfiguration propertyConfiguration;

	public List<ApiKeyAdditionalDetail> getDetails(String authorization) throws ActivityException {
		String stringActivity =  webClient.get().uri(URI.create(propertyConfiguration.getApiKeyDetailsUrl() + authorization))
				.accept(MediaType.APPLICATION_JSON).header(HttpHeaders.AUTHORIZATION, AuthUtil.authToken).retrieve()
				.bodyToMono(String.class).block();
		return getModelFromJson(stringActivity);
	}

	private List<ApiKeyAdditionalDetail> getModelFromJson(String json) {
		Gson objGson = new Gson();
		Type listType = new TypeToken<List<ApiKeyAdditionalDetail>>() {
		}.getType();
		return objGson.fromJson(json, listType);
	}

	public String getDetailsWithKey(String authorization, String key) throws ActivityException {
		List<ApiKeyAdditionalDetail> detailsList = webClient.get()
				.uri(URI.create(propertyConfiguration.getApiKeyDetailsUrl() + authorization + "?Key=" + key))
				.accept(MediaType.APPLICATION_JSON)
				.header(HttpHeaders.AUTHORIZATION, authorization)
				.retrieve()
				.onStatus(statusPredicate -> !statusPredicate.is2xxSuccessful(),
						error -> Mono.error(new RuntimeException(error.toString())))
				.bodyToFlux(ApiKeyAdditionalDetail.class).collectList().block();
		return detailsList.get(0).getValue();

	}
}
