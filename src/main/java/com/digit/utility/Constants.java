package com.digit.utility;
/**
 * 
 * @author Lakshman.Doke
 *
 */
public class Constants {
	public static final String DOMESTIC_TRAVEL_TYPE = "D";
	public static final String LESIURE_TRAVEL = "L";
	public static final String NATURAL_PERSON_TYPE = "N";
	public static final String C9_PACKAGE_NAME = "C9";
	public static final String CN_PACKAGE_NAME = "CN";
	public static final String GEOGRAPHY = "ASA";
	public static final String TRAVEL_COUNTRY = "IN";
	public static final String NOMINEE_TYPE_OTHER = "X";
	public static final String AIR_TRAVEL_MODE = "A";
	public static final String FARE_TYPE = "B";
	public static final String DEFAULT_BIRTHDATE = "1990-01-01";
	public static final String SUCCESS = "Success";
	public static final String LEGAL_PERSON_TYPE = "L";
	public static final String GST_DOCUMENT_TYPE = "GST";
	public static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
	public static final String EXTRA_SECONDS = ":00";
	public static final String CLASS_PRODUCT_CODE_DELAY = "RR";
	public static final String COMMON_PRODCUT_COMPONENT_NUMBER = "47869";
	public static final String ONE_WAY_PRODCUT_COMPONENT_NUMBER = "47871";
	public static final String RETURN_PRODCUT_COMPONENT_NUMBER = "47870";
	public static final String ONE_WAY_JOURNEY = "One Way";
	public static final String ONWARDS_JOURNEY = "onward";
	public static final String RETURN_JOURNEY = "return";
	public static final String INDIGO_AIRLINES = "6E";
	public static final String JET_AIRLINES = "9W";
	public static final String JET_INDIGO_BEFORE_NOON_PRODUCT_SEQUENCE = "47872";
	public static final String JET_INDIGO_AFTER_NOON_PRODUCT_SEQUENCE = "47873";
	public static final String OTHER_BEFORE_NOON_PRODUCT_SEQUENCE = "47874";
	public static final String OTHER_AFTER_NOON_PRODUCT_SEQUENCE = "47875";
	public static final String FIVE_HUNDRED_COVER = "INR 500";
	public static final String SEVEN_FIFTY_COVER = "INR 750";
	public static final String NAME_BOOKING = "Name-Booking";
	public static final String EMAIL = "Email";
	public static final String MOBILE = "Mobile";
	public static final String GST = "gst";
	public static final String STREET = "street";
	public static final String CITY = "city";
	public static final String PIN = "pin";
	public static final String STATE = "state";
	public static final String ADDRESS = "Address";
	public static final String DUMMY_PNR = "dummy";
	public static final String PLUS_CLAIM_DAYS = "PLUS_CLAIM_DAYS";
	public static final String MINUS_CLAIM_DAYS = "MINUS_CLAIM_DAYS";
	public static final String CLAIM_TYPE = "CTAMCL";
	public static final String ECONOMY_TRAVEL_CLASS = "Economy";
	public static final String PASSENGER_NAME_MISMATCH = "Passenger name mismatch";
	public static final String NON_ECONOMY_MISMATCH = "New flight can only be economy class";
	public static final String CLAIMED_FLIGHT = "Already claimed for this flight";
	public static final String LESS_THAN_TWO_HOURS_FLIGHT = "Cannot claim prior to two hours of original flight";
	public static final String SOURCE_MISMATCH = "Source mismatch";
	public static final String DESTINATION_MISMATCH = "Destination mismatch";
	public static final String AMMENDMENT_PERIOD_MISMATCH = "Ammendment period mismatch";
	public static final String AITRLINE_CANCELATION_AMOUNT_ERROR = "Airline cancelaiton amount cannot be null or empty";
	public static final String CT_CANCELATION_AMOUNT_ERROR = "CT cancelaiton amount cannot be null or empty";
}
