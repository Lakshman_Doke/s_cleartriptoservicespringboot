package com.digit.utility;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.digit.dpsModels.Address;
import com.digit.dpsModels.Customer;
import com.digit.dpsModels.Document;
import com.digit.dpsModels.FlightDetails;
import com.digit.dpsModels.Nominee;
import com.digit.dpsModels.Policy;
import com.digit.entities.BookingData;
import com.digit.entities.Flight;
import com.digit.entities.Passenger;
import com.digit.enums.CABIN_TYPE;
import com.digit.enums.GENDER;
import com.digit.enums.JOURNEY_TYPE;
import com.digit.model.ApiKeyAdditionalDetail;
import com.digit.model.Booking;
import com.digit.model.Coverage;
import com.digit.model.ProductComponent;
import com.digit.model.SubProductComponent;
import com.digit.service.QuickQuoteService;
import com.google.gson.Gson;

/**
 * 
 * @author Lakshman.Doke
 *
 */
@Component
public class BookingToDps {
	private static final Logger logger = LoggerFactory.getLogger(BookingToDps.class);
	QuickQuoteService quickQuoteService;
	@Autowired
	FleetNumberUtilController fleetNumberUtil;
	AbsDataServiceClient absDataServiceClient = new AbsDataServiceClient();
	@Autowired
	private PropertyConfiguration propertyConfiguration;
	Gson gson = new Gson();

	@Autowired
	CoverCode coverCode;

	public Policy convertJson(Booking booking) {
		var bookingData = booking.getBooking();
		var policy = new Policy();
		policy.setBookingId(bookingData.getTripId());
		policy.setPurpose(Constants.LESIURE_TRAVEL);
		policy.setPackageName(bookingData.getSubInsuranceProductCode());
		policy.setGeography(Constants.GEOGRAPHY);
		policy.setCountryTravel(Constants.TRAVEL_COUNTRY);
		policy.setType(Constants.DOMESTIC_TRAVEL_TYPE);
		policy.setInsuredPersonsCount(String.valueOf(bookingData.getPassengers().size()));
		policy.setTravelStartDate(
				bookingData.getPassengers().get(0).getFlights().get(0).getDepartureDate().split("T")[0]);
		policy.setTravelEndDate(bookingData.getPassengers().get(0).getFlights()
				.get(bookingData.getPassengers().get(0).getFlights().size() - 1).getArrivalDate().split("T")[0]);

		var startDate = LocalDate.parse(policy.getTravelStartDate());
		var endDate = LocalDate.parse(policy.getTravelEndDate());

		if (startDate.isAfter(endDate)) {
			policy.setTravelStartDate(endDate.toString());
			policy.setTravelEndDate(startDate.toString());
		}
		policy.setBookingDate(bookingData.getBookingDate());
		policy.setQuoteId(bookingData.getQuoteId());

		var dpsAddress = new Address();

		var address = coverCode
				.getAddressByAirportCode(bookingData.getPassengers().get(0).getFlights().get(0).getOrigin());

		if (address == null) {
			address = new com.digit.model.Address();
			address.setCity("");
			address.setPin("");
			address.setState("");
			address.setCountry("");
		}
		dpsAddress.setLine1("");
		dpsAddress.setCity(address.getCity());
		dpsAddress.setState(address.getState());
		dpsAddress.setPinCode(address.getPin());
		dpsAddress.setCountry("IND");

		List<ApiKeyAdditionalDetail> additionalDetailsList = null;
		try {
			additionalDetailsList = fleetNumberUtil.getDetails(propertyConfiguration.getApiKey());
		} catch (Exception e) {
			logger.error("convertJson(Booking booking): Exception: {}", e.getMessage());
		}
		Map<String, String> detailsMap = new HashMap<>();

		if (additionalDetailsList != null)
			for (ApiKeyAdditionalDetail apiKeyAdditionalDetail : additionalDetailsList) {
				detailsMap.put(apiKeyAdditionalDetail.getKey(), apiKeyAdditionalDetail.getValue());
			}

		var customer = new Customer();
		customer.setTitle(bookingData.getCustomer().getTitle());
		customer.setFirstName(bookingData.getCustomer().getFirstName());
		customer.setLastName(bookingData.getCustomer().getLastName());
		if (customer.getCompanyName() == null || !customer.getCompanyName().trim().isEmpty()) {
			customer.setType(Constants.LEGAL_PERSON_TYPE);
			this.getDoc(null, customer, detailsMap.get(Constants.GST));
			customer.setFirstName(null);
			customer.setLastName(null);
			customer.setCompanyName(detailsMap.get(Constants.NAME_BOOKING));
			customer.setEmail(detailsMap.get(Constants.EMAIL));
			customer.setMobile(detailsMap.get(Constants.MOBILE));
			this.getLegalAddress(null, customer, detailsMap.get(Constants.ADDRESS));
		} else {
			customer.setType(Constants.LEGAL_PERSON_TYPE);
			this.getDoc(bookingData.getCustomer(), customer, null);
			customer.setCompanyName(bookingData.getCustomer().getCompanyName());
			customer.setFirstName(bookingData.getCustomer().getCompanyName());
			this.getLegalAddress(bookingData.getCustomer(), customer, null);
		}

		if (bookingData.getCustomer().getDOB() != null && !bookingData.getCustomer().getDOB().trim().isEmpty())
			customer.setDob(bookingData.getCustomer().getDOB().substring(0, 10));
		else if (customer.getType().equals(Constants.NATURAL_PERSON_TYPE))
			customer.setDob(Constants.DEFAULT_BIRTHDATE);

		policy.setCustomer(customer);

		if (bookingData.getQuoteId() != null)
			policy.setQuoteId(bookingData.getQuoteId());

		List<Passenger> passengersList = bookingData.getPassengers();

		List<Passenger> newPassengers = new ArrayList<>();
		List<Customer> insuredPersons = new ArrayList<>();
		var counter = 0;
		for (Passenger passenger : passengersList) {

			var insuredPerson = new Customer();

			insuredPerson.setFirstName(passenger.getFirstName());
			insuredPerson.setLastName(passenger.getLastName());
			insuredPerson.setGender(GENDER.valueOf(passenger.getGender()).toString());
			insuredPerson.setTitle(passenger.getTitle());

			if (passenger.getDOB() != null && !passenger.getDOB().trim().isEmpty())
				insuredPerson.setDob(passenger.getDOB().split("T")[0]);

			insuredPerson.setMobile(passenger.getMobile());
			insuredPerson.setEmail(passenger.getEmail());
			if (passenger.getPaxId() == null) {
				insuredPerson.setPaxId(String.valueOf(counter++));
				passenger.setPaxId(insuredPerson.getPaxId());
			} else {
				insuredPerson.setPaxId(passenger.getPaxId());
			}

			if (passenger.getDOB() != null && !passenger.getDOB().trim().isEmpty())
				insuredPerson.setDob(passenger.getDOB().substring(0, 10));
			else
				insuredPerson.setDob(Constants.DEFAULT_BIRTHDATE);

			var nominee = new Nominee();

			nominee.setName(insuredPerson.getFirstName());

			nominee.setRelationship(Constants.NOMINEE_TYPE_OTHER);

			insuredPerson.setNominee(nominee);
			insuredPerson.setType(Constants.NATURAL_PERSON_TYPE);

			insuredPerson.setAddress(dpsAddress);

			List<Flight> requestFlight = passenger.getFlights();
			List<FlightDetails> flights = new ArrayList<>();
			for (Flight flight : requestFlight) {
				var flightDetails = new FlightDetails();

				if (bookingData.getQuoteId() != null) {
					flightDetails.setBaseFare(String.valueOf(flight.getFare()));
				}

				flightDetails.setJourneyType(JOURNEY_TYPE.valueOf(flight.getJourneyType()).toString());
				flightDetails.setLeg(flight.getLeg());
				flightDetails.setSource(flight.getOrigin());
				flightDetails.setDestination(flight.getDestination());
				flightDetails.setStartDate(flight.getDepartureDate().substring(0, 19));
				flightDetails.setEndDate(flight.getArrivalDate().substring(0, 19));
				flightDetails.setProviderName(flight.getCarrierNo());
				flightDetails.setProviderTravelNo(flight.getCarrierFlightNo());
				flightDetails.setClassType(CABIN_TYPE.Economy.toString());
				if (flight.getAirlinePNR() != null && !flight.getAirlinePNR().isEmpty())
					flightDetails.setTicketNumber(flight.getAirlinePNR());
				else
					flightDetails.setTicketNumber(Constants.DUMMY_PNR);

				flightDetails.setTravelMode(Constants.AIR_TRAVEL_MODE);
				flightDetails.setFareType(Constants.FARE_TYPE);

				if (flightDetails.getBaseFare() == null)
					flightDetails.setBaseFare("0");

				flightDetails.setCancellationCharge("0");
				flightDetails.setFeesSurcharge("0");
				flightDetails.setConvenienceFee("0");
				flights.add(flightDetails);
			}
			insuredPerson.setFlights(flights);
			insuredPersons.add(insuredPerson);
			newPassengers.add(passenger);
		}
		policy.setInsuredPersons(insuredPersons);
		bookingData.setPassengers(newPassengers);
		booking.setBooking(bookingData);

		if (policy.getPackageName().equalsIgnoreCase(Constants.C9_PACKAGE_NAME)
				|| policy.getPackageName().equalsIgnoreCase(Constants.CN_PACKAGE_NAME)) {
			this.getCoverages(policy, bookingData);
		}
		logger.info("convertJson(Booking booking): Policy: {}", gson.toJson(policy));
		return policy;
	}

	public void getDoc(com.digit.entities.Customer customer, Customer absCustomer, String gst) {
		List<Document> documents = new ArrayList<>();
		var document = new Document();
		document.setType(Constants.GST_DOCUMENT_TYPE);
		if (gst == null)
			document.setId(customer!=null ?customer.getCustomerGSTIN():"");
		else
			document.setId(gst);
		documents.add(document);
		absCustomer.setDocuments(documents);
	}

	public void getLegalAddress(com.digit.entities.Customer customer, Customer absCustomer, String apiAddress) {
		var address = new Address();

		if (apiAddress == null) {
			address.setLine1(customer.getAddress1());
			address.setLine2(customer.getAddress2());
			address.setCity(customer.getCity());
			address.setState(customer.getState());
			address.setPinCode(customer.getPinCode());
			address.setCountry(customer.getCountry());
		} else {
			Map<String, String> addressMap = new HashMap<>();
			String[] addressArray = apiAddress.split(":");
			for (String s : addressArray) {
				String[] elementSplit = s.split("\\|");
				addressMap.put(elementSplit[0], elementSplit[1]);
			}

			address.setLine1(addressMap.get(Constants.STREET));
			address.setCity(addressMap.get(Constants.CITY));
			address.setPinCode(addressMap.get(Constants.PIN));
			address.setState(addressMap.get(Constants.STATE));
			address.setCountry(Constants.TRAVEL_COUNTRY);
		}

		absCustomer.setAddress(address);
	}

	public void getCoverages(Policy policy, BookingData bookingData) {

		var coverage = new Coverage();
		List<Coverage> domesticCoverages = new ArrayList<>();
		List<ProductComponent> productComponents = new ArrayList<>();

		coverage.setClassProductCode(Constants.CLASS_PRODUCT_CODE_DELAY);

		List<Flight> flights = bookingData.getPassengers().get(0).getFlights();

		flights.forEach(flight -> {
			List<SubProductComponent> subProductComponents = new ArrayList<>();
			var innerSubProductComponent = new SubProductComponent();
			List<SubProductComponent> innerSubProdcutComponents = new ArrayList<>();
			var productComponent = new ProductComponent();
			productComponent.setProductComponentNumber(Constants.COMMON_PRODCUT_COMPONENT_NUMBER);
			var subProductComponent = new SubProductComponent();

			if (bookingData.getTripType().equalsIgnoreCase(Constants.ONE_WAY_JOURNEY)) {
				subProductComponent.setProductComponentNumber(Constants.ONE_WAY_PRODCUT_COMPONENT_NUMBER);
			} else {
				subProductComponent.setProductComponentNumber(Constants.RETURN_PRODCUT_COMPONENT_NUMBER);
			}

			if (flight != null && flight.getCarrierName() != null) {

				var departureTime = LocalDateTime.parse(flight.getDepartureDate().substring(0, 19));
				if (flight.getCarrierNo().equalsIgnoreCase(Constants.INDIGO_AIRLINES)
						|| flight.getCarrierNo().equalsIgnoreCase(Constants.JET_AIRLINES)) {
					if (departureTime.getHour() < 12)
						innerSubProductComponent
								.setProductComponentNumber(Constants.JET_INDIGO_BEFORE_NOON_PRODUCT_SEQUENCE);
					else
						innerSubProductComponent
								.setProductComponentNumber(Constants.JET_INDIGO_AFTER_NOON_PRODUCT_SEQUENCE);

				} else {
					if (departureTime.getHour() < 12)
						innerSubProductComponent
								.setProductComponentNumber(Constants.OTHER_BEFORE_NOON_PRODUCT_SEQUENCE);
					else
						innerSubProductComponent.setProductComponentNumber(Constants.OTHER_AFTER_NOON_PRODUCT_SEQUENCE);
				}
				if (departureTime.getMonthValue() == 12 || departureTime.getMonthValue() == 1)
					innerSubProductComponent.setSumAmount(Constants.FIVE_HUNDRED_COVER);
				else
					innerSubProductComponent.setSumAmount(Constants.SEVEN_FIFTY_COVER);
			}
			innerSubProdcutComponents.add(innerSubProductComponent);
			subProductComponent.setSubProductComponents(innerSubProdcutComponents);
			subProductComponents.add(subProductComponent);
			productComponent.setSubProductComponents(subProductComponents);
			productComponents.add(productComponent);
		});
		coverage.setProductComponents(productComponents);
		domesticCoverages.add(coverage);
		policy.setDomesticCoverages(domesticCoverages);
	}

	public void convertPolicyToBooking(Policy policy, Booking booking) {

		List<Customer> insuredPersons = policy.getInsuredPersons();

		if (!booking.getBooking().getSubInsuranceProductCode().equals(Constants.C9_PACKAGE_NAME)
				&& !booking.getBooking().getSubInsuranceProductCode().equals(Constants.CN_PACKAGE_NAME)) {

			if (policy !=null && policy.getStatus() !=null && policy.getStatus().getCode().equals("200") && insuredPersons != null && !insuredPersons.isEmpty()) {
				booking.getBooking().setDIGITPolicyNo(policy.getInsuredPersons().get(0).getPolicyID());
				booking.getBooking().setUpdateStatus(Constants.SUCCESS);
				for (Passenger passenger : booking.getBooking().getPassengers()) {
					passenger.setPolicyNumber(policy.getInsuredPersons().get(0).getPolicyID());
					passenger.setIsOnwardsClaimed(false);
					if (booking.getBooking().getTripType().equalsIgnoreCase(Constants.RETURN_JOURNEY))
						passenger.setIsReturnClaimed(false);
				}
			} else {
				booking.getBooking().setDIGITPolicyNo("NA");
				booking.getBooking().setUpdateStatus(policy.getStatus().getMessage());
				for (Passenger passenger : booking.getBooking().getPassengers()) {
					passenger.setPolicyNumber("NA");
				}
			}
			return;
		}

		List<Passenger> newPassengers = new ArrayList<>();
		if (policy.getStatus().getCode().equals("200")) {
			for (Customer customer : insuredPersons) {
				for (Passenger passenger : booking.getBooking().getPassengers()) {
					List<Flight> newFlights = new ArrayList<>();
					if (passenger.getPaxId().equals(customer.getPaxId())) {
						passenger.setPolicyNumber(customer.getPolicyID());
						List<Flight> flights = passenger.getFlights();
						for (Flight flight : flights) {
							flight.setDIGITPolicyNo(customer.getPolicyID());
							flight.setUpdateStatus(Constants.SUCCESS);
							newFlights.add(flight);
						}
						passenger.setFlights(newFlights);
						newPassengers.add(passenger);
					}
				}

			}
		} else {
			for (Passenger passenger : booking.getBooking().getPassengers()) {
				List<Flight> newFlights = new ArrayList<>();

				List<Flight> flights = passenger.getFlights();
				passenger.setPolicyNumber("NA");
				for (Flight flight : flights) {
					flight.setDIGITPolicyNo("NA");
					flight.setUpdateStatus(policy.getStatus().getMessage());
					newFlights.add(flight);
				}
				passenger.setFlights(newFlights);
				newPassengers.add(passenger);
			}
		}
		booking.getBooking().setPassengers(newPassengers);
	}
}
