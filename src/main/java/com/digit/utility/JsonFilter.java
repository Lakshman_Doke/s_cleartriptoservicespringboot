package com.digit.utility;

import java.util.List;

import com.digit.entities.BookingData;
import com.digit.entities.Customer;
import com.digit.entities.Flight;
import com.digit.entities.Passenger;
import com.digit.entities.Payment;
import com.digit.model.Booking;
/**
 * 
 * @author Lakshman.Doke
 *
 */
public class JsonFilter {
    public static Booking filter(Booking bookings) {

        BookingData Booking = bookings.getBooking();
        List<Passenger> Passengers = Booking.getPassengers();
        if (Passengers != null)
            for (int i = 0; i < Passengers.size(); i++) {
                Passengers.get(i).setBookingData(Booking);
                List<Flight> Flights = Passengers.get(i).getFlights();
                for (int j = 0; j < Flights.size(); j++) {
                    Flights.get(j).setPassenger(Passengers.get(i));
                }
                Passengers.get(i).setFlights(Flights);
            }
        Booking.setPassengers(Passengers);

        Customer Customer = Booking.getCustomer();

        Customer.setBookingData(Booking);
        Booking.setCustomer(Customer);

        Payment Payment = Booking.getPayment();

        Payment.setBookingData(Booking);
        Booking.setPayment(Payment);

        bookings.setBooking(Booking);
        return bookings;
    }

}
