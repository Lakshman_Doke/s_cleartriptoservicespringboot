package com.digit.utility;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import com.digit.dpsModels.Policy;
import com.digit.exception.PolicyCreationException;
import com.digit.model.PolicyStatus;
import com.google.gson.Gson;

import reactor.core.publisher.Mono;

/**
 * 
 * @author Lakshman.Doke
 *
 */
@Component
public class DpsClient {
	private static final Logger logger = LoggerFactory.getLogger(DpsClient.class);
	Gson gson = new Gson();
	@Autowired
	PropertyConfiguration propertyConfiguration;

	public Policy callDps(Policy policy, String apiKey) throws PolicyCreationException {
		PolicyStatus policyStatus = new PolicyStatus();
		logger.info("callDps(Policy policy, String apiKey): Policy obj: {}",
				policy != null ? new Gson().toJson(policy) : null);
		ResponseEntity<String> policyResponse;
		try {
			policyResponse = WebClient.create(propertyConfiguration.getCreateAmendmentPolicyUrl()).post()
					.accept(MediaType.APPLICATION_JSON).header(HttpHeaders.AUTHORIZATION, apiKey)
					.body(Mono.just(policy), Policy.class)
					.retrieve()
					.onStatus(HttpStatus::isError,
							response -> response.bodyToMono(String.class)
									.flatMap(error -> Mono.error(new RuntimeException(String.valueOf(error)))))
					.toEntity(String.class)
					.block();

			logger.info("callDps(Policy policy, String apiKey): Resepose from DPS for booking id: {}  ",
					policy.getBookingId());
			logger.info("callDps(Policy policy, String apiKey): Resepose {}",
					(policyResponse != null ? policyResponse : ""));
			String returnPolicyString = policyResponse != null ? policyResponse.getBody() : "";
			Policy returnPolicy = gson.fromJson(returnPolicyString, Policy.class);
			policyStatus.setCode("200");
			policyStatus.setMessage("Success");
			returnPolicy.setStatus(policyStatus);
			return returnPolicy;
		} catch (Exception exception) {
			logger.error("callDps(Policy policy, String apiKey): Exception:{0}", exception);
			return gson.fromJson(exception.getMessage(), Policy.class);
		}
	}
}
