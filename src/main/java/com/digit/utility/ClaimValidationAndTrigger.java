package com.digit.utility;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.digit.entities.BookingData;
import com.digit.entities.Flight;
import com.digit.entities.Passenger;
import com.digit.exception.ActivityException;
import com.digit.exception.PolicyCreationException;
import com.digit.model.ApiKeyAdditionalDetail;
import com.digit.repositories.BookingDataRepository;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
/**
 * 
 * @author Lakshman.Doke
 *
 */
@Component
public class ClaimValidationAndTrigger {
	@Autowired
	FleetNumberUtilController fleetNumberUtilController;
	@Autowired
	AbsDataServiceClient absDataServiceClient;

	@Autowired
	PropertyConfiguration propertyConfiguration;
	Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();

	@Autowired
	BookingDataRepository bookingDataRepository;

	private static final Logger logger = LoggerFactory.getLogger(ClaimValidationAndTrigger.class);

	public Boolean compareNewAndStoredBooking(BookingData storedBooking, BookingData newBooking) {
		List<Passenger> storedPassengers = storedBooking.getPassengers();
		List<Passenger> newPassengers = newBooking.getPassengers();
		String apiKey = propertyConfiguration.getApiKey();

		Long maxDays = null;
		Long minDays = null;

		List<ApiKeyAdditionalDetail> additionalDetailsList = null;
		try {
			additionalDetailsList = fleetNumberUtilController.getDetails(apiKey);
		} catch (ActivityException e) {
			logger.error("compareNewAndStoredBooking(BookingData storedBooking, BookingData newBooking):Exception: ",
					e);
		}

		for (ApiKeyAdditionalDetail apiKeyAdditionalDetail : additionalDetailsList) {
			if (apiKeyAdditionalDetail.getKey().equals(Constants.PLUS_CLAIM_DAYS)) {
				maxDays = Long.parseLong(apiKeyAdditionalDetail.getValue());
			} else if (apiKeyAdditionalDetail.getKey().equals(Constants.MINUS_CLAIM_DAYS)) {
				minDays = Long.parseLong(apiKeyAdditionalDetail.getValue());
			}
		}
		if (maxDays == null && minDays == null) {
			maxDays = 60l;
			minDays = -10l;
		}

		List<Passenger> tempPassenger = new ArrayList<>();
		List<Passenger> storedTempPassengers = new ArrayList<>();
		int ineligibleFlight = 0;
		int eligibleFlight = 0;
		for (Passenger newPassenger : newPassengers) {
			for (Passenger storedPassenger : storedPassengers) {
				if (newPassenger.getPaxId().equals(storedPassenger.getPaxId())) {
					if (!newPassenger.getFirstName().equalsIgnoreCase(storedPassenger.getFirstName())
							|| !newPassenger.getLastName().equalsIgnoreCase(storedPassenger.getLastName())) {
						newPassenger.setStatus(Constants.PASSENGER_NAME_MISMATCH);
						ineligibleFlight++;
						continue;
					} else {
						if ((newPassenger.getPolicyNumber() == null || newPassenger.getPolicyNumber().trim().isEmpty()
								|| newPassenger.getPolicyNumber().equals("NA"))
								&& (storedPassenger.getPolicyNumber() != null
										&& !storedPassenger.getPolicyNumber().trim().isEmpty()
										&& !storedPassenger.getPolicyNumber().equals("NA"))) {
							newPassenger.setPolicyNumber(storedPassenger.getPolicyNumber());
						}

						List<Flight> newFlights = newPassenger.getFlights();

						List<Flight> storedFlights = storedPassenger.getFlights();

						List<Flight> tempFlights = new ArrayList<>();
						String storedOnwardsSource = null;
						String storedOnwardsDestination = null;
						String storedReturnSource = null;
						String storedReturnDestination = null;
						String newOnwardsSource = null;
						String newOnwardsDestination = null;
						String newReturnSource = null;
						String newReturnDestination = null;

						int onwardsLeg = 0;
						int returnLeg = 0;
						for (Flight flight : storedFlights) {
							if (flight.getJourneyType().equalsIgnoreCase(Constants.ONWARDS_JOURNEY)) {
								onwardsLeg++;
								if (Integer.parseInt(flight.getLeg()) == 1) {
									storedOnwardsSource = flight.getOrigin();
									storedOnwardsDestination = flight.getDestination();
								} else if (onwardsLeg > 1) {
									storedOnwardsDestination = flight.getDestination();
								}
							} else if (flight.getJourneyType().equalsIgnoreCase(Constants.RETURN_JOURNEY)) {
								returnLeg++;
								if (Integer.parseInt(flight.getLeg()) == 1) {
									storedReturnSource = flight.getOrigin();
									storedReturnDestination = flight.getDestination();
								} else if (returnLeg > 1) {
									storedReturnDestination = flight.getDestination();
								}
							}
						}

						for (Flight flight : newFlights) {
							if (flight.getJourneyType().equalsIgnoreCase(Constants.ONWARDS_JOURNEY)) {
								onwardsLeg++;
								if (Integer.parseInt(flight.getLeg()) == 1) {
									newOnwardsSource = flight.getOrigin();
									newOnwardsDestination = flight.getDestination();
								} else if (onwardsLeg > 1) {
									newOnwardsDestination = flight.getDestination();
								}
							} else if (flight.getJourneyType().equalsIgnoreCase(Constants.RETURN_JOURNEY)) {
								returnLeg++;
								if (Integer.parseInt(flight.getLeg()) == 1) {
									newReturnSource = flight.getOrigin();
									newReturnDestination = flight.getDestination();
								} else if (returnLeg > 1) {
									newReturnDestination = flight.getDestination();
								}
							}
						}

						for (Flight storedFlight : storedFlights) {
							for (Flight newFlight : newFlights) {
								if (!newFlight.getCabinType().equalsIgnoreCase(Constants.ECONOMY_TRAVEL_CLASS)
										&& !newFlight.getCabinType().equalsIgnoreCase("E")) {
									newFlight.setUpdateStatus(Constants.NON_ECONOMY_MISMATCH);
									newFlight.setPassenger(storedPassenger);
									newPassenger.setBookingData(storedBooking);
									ineligibleFlight++;
									continue;
								}

								if ((storedFlight.getJourneyType().equalsIgnoreCase(Constants.ONWARDS_JOURNEY)
										&& newFlight.getJourneyType().equalsIgnoreCase(Constants.ONWARDS_JOURNEY)
										&& storedPassenger.getIsOnwardsClaimed() != null
										&& storedPassenger.getIsOnwardsClaimed())
										|| (storedFlight.getJourneyType().equalsIgnoreCase(Constants.RETURN_JOURNEY)
												&& newFlight.getJourneyType().equalsIgnoreCase(Constants.RETURN_JOURNEY)
												&& storedPassenger.getIsReturnClaimed() != null
												&& storedPassenger.getIsReturnClaimed())) {
									newFlight.setUpdateStatus(Constants.CLAIMED_FLIGHT);
									newFlight.setPassenger(storedPassenger);
									newPassenger.setBookingData(storedBooking);
									ineligibleFlight++;
									continue;
								}

								if ((storedFlight.getJourneyType().equalsIgnoreCase(Constants.ONWARDS_JOURNEY)
										&& newFlight.getJourneyType().equalsIgnoreCase(Constants.ONWARDS_JOURNEY))
										|| (storedFlight.getJourneyType().equalsIgnoreCase(Constants.RETURN_JOURNEY)
												&& newFlight.getJourneyType()
														.equalsIgnoreCase(Constants.RETURN_JOURNEY))) {

									LocalDateTime storedScheduleDeparture = LocalDateTime
											.parse(storedFlight.getDepartureDate().substring(0, 19));
									LocalDateTime currentDateTime = LocalDateTime.now();

									long diffMins = currentDateTime.until(storedScheduleDeparture, ChronoUnit.MINUTES);

									if (diffMins < 120) {
										ineligibleFlight++;
										newFlight.setUpdateStatus(Constants.LESS_THAN_TWO_HOURS_FLIGHT);
										newFlight.setPassenger(storedPassenger);
										newPassenger.setBookingData(storedBooking);
										continue;
									}

									if (newFlight.getJourneyType().equalsIgnoreCase(Constants.ONWARDS_JOURNEY)) {
										if (storedOnwardsSource != null && newOnwardsSource != null
												&& !storedOnwardsSource.equals(newOnwardsSource)) {
											newFlight.setUpdateStatus(Constants.SOURCE_MISMATCH);
											ineligibleFlight++;
											newFlight.setPassenger(storedPassenger);
											newPassenger.setBookingData(storedBooking);
											continue;
										}
										if (storedOnwardsDestination != null && newOnwardsDestination != null
												&& !storedOnwardsDestination.equalsIgnoreCase(newOnwardsDestination)) {
											newFlight.setUpdateStatus(Constants.DESTINATION_MISMATCH);
											ineligibleFlight++;
											newFlight.setPassenger(storedPassenger);
											newPassenger.setBookingData(storedBooking);
											continue;
										}
									} else if (newFlight.getJourneyType().equalsIgnoreCase(Constants.RETURN_JOURNEY)) {
										if (storedReturnSource != null && newReturnSource != null
												&& !storedReturnSource.equals(newReturnSource)) {
											newFlight.setUpdateStatus(Constants.SOURCE_MISMATCH);
											newFlight.setPassenger(storedPassenger);
											newPassenger.setBookingData(storedBooking);
											ineligibleFlight++;
											continue;
										}
										if (storedReturnDestination != null && newReturnDestination != null
												&& !storedReturnDestination.equalsIgnoreCase(newReturnDestination)) {
											newFlight.setUpdateStatus(Constants.DESTINATION_MISMATCH);
											newFlight.setPassenger(storedPassenger);
											newPassenger.setBookingData(storedBooking);
											ineligibleFlight++;
											continue;
										}
									}

									LocalDate newDepartureDate = LocalDate.parse(newFlight.getDepartureDate()
											.substring(0, newFlight.getDepartureDate().indexOf('T')));
									LocalDate storedScheduledDate = LocalDate.parse(storedFlight.getDepartureDate()
											.substring(0, storedFlight.getDepartureDate().indexOf('T')));
									long daysDiff = storedScheduledDate.until(newDepartureDate, ChronoUnit.DAYS);

									if (daysDiff > maxDays || daysDiff < minDays) {
										newFlight.setUpdateStatus(Constants.AMMENDMENT_PERIOD_MISMATCH);
										newFlight.setPassenger(storedPassenger);
										newPassenger.setBookingData(storedBooking);
										ineligibleFlight++;
										continue;
									}

									newFlight.setOriginalAirlinePNR(storedFlight.getAirlinePNR());
									newFlight.setOriginalCarrierFlightNo(storedFlight.getCarrierFlightNo());
									newFlight.setOriginalCarrierName(storedFlight.getCarrierName());
									newFlight.setOriginalCarrierNo(storedFlight.getCarrierNo());

									if (newFlight.getALCancellationAmt() == null
											|| newFlight.getALCancellationAmt().isEmpty()) {
										newFlight.setUpdateStatus(Constants.AITRLINE_CANCELATION_AMOUNT_ERROR);
										newFlight.setPassenger(storedPassenger);
										newPassenger.setBookingData(storedBooking);
										ineligibleFlight++;
										continue;
									}

									if (newFlight.getCTCancellationAmt() == null
											|| newFlight.getCTCancellationAmt().isEmpty()) {
										newFlight.setUpdateStatus(Constants.CT_CANCELATION_AMOUNT_ERROR);
										newFlight.setPassenger(storedPassenger);
										newPassenger.setBookingData(storedBooking);
										ineligibleFlight++;
										continue;
									}
									double loadFactor = 0;
									loadFactor = storedFlight.getFare() - newFlight.getFare();
									double claimAmmount = Math.min(storedFlight.getFare(),
											Double.parseDouble(newFlight.getALCancellationAmt()));

									claimAmmount = Math.min(claimAmmount, 3000);
									claimAmmount += Double.parseDouble(newFlight.getCTCancellationAmt());

									if (loadFactor > 0)
										claimAmmount -= loadFactor;

									claimAmmount = Math.max(claimAmmount, 0);

									newFlight.setLoadFactor(loadFactor);
									newFlight.setOldFare(storedFlight.getFare());
									newFlight.setNewFare(newFlight.getFare());
									newFlight.setClaimAmmount(claimAmmount);
									newFlight.setPassenger(storedPassenger);

									if (storedFlight.getJourneyType().equalsIgnoreCase(Constants.ONWARDS_JOURNEY)
											&& newFlight.getJourneyType().equalsIgnoreCase(Constants.ONWARDS_JOURNEY))
										storedPassenger.setIsOnwardsClaimed(true);
									else if (storedFlight.getJourneyType().equalsIgnoreCase(Constants.RETURN_JOURNEY)
											&& newFlight.getJourneyType().equalsIgnoreCase(Constants.RETURN_JOURNEY))
										storedPassenger.setIsReturnClaimed(true);

									newFlight.setUpdateStatus(Constants.SUCCESS);

									eligibleFlight++;
								}
								tempFlights.add(newFlight);
							}

						}
						storedPassenger.getFlights().addAll(tempFlights);

						newPassenger.setFlights(newFlights);
					}
				}
				storedTempPassengers.add(storedPassenger);
			}
			if (!newPassenger.getFlights().isEmpty())
				tempPassenger.add(newPassenger);
		}
		newBooking.setPassengers(tempPassenger);
		if (!storedPassengers.isEmpty())

			storedBooking.setPassengers(storedTempPassengers);
		storedBooking.setLastModifiedDate(Timestamp.valueOf(LocalDateTime.now()));
		bookingDataRepository.save(storedBooking);
		if (ineligibleFlight > 0 && eligibleFlight > 0) {
			newBooking.setUpdateStatus(RETURN_STATUS.PARTIAL.toString());
		} else if (ineligibleFlight == 0 && eligibleFlight > 0) {
			newBooking.setUpdateStatus(RETURN_STATUS.SUCCESS.toString());
		} else if (ineligibleFlight > 0 && eligibleFlight == 0) {
			newBooking.setUpdateStatus(RETURN_STATUS.INELIGIBLE.toString());
		}

		return eligibleFlight > 0;
	}

	public void triggerClaim(BookingData newBookingData) throws PolicyCreationException {
		if (newBookingData.getDIGITPolicyNo() == null && newBookingData.getPassengers() != null) {
			newBookingData.setDIGITPolicyNo(newBookingData.getPassengers().get(0).getPolicyNumber());
		}
		List<BookingData> newBookingList = new ArrayList<>();
		newBookingList.add(newBookingData);
		String json = gson.toJson(newBookingList);

		logger.info("triggerClaim(BookingData newBookingData): Triggering claim for: ", json);

		absDataServiceClient.callAbsDataService(json);

	}
}
