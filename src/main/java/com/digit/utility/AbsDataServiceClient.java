package com.digit.utility;

import com.digit.entities.BookingData;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import reactor.core.publisher.Flux;

import java.util.List;

/**
 * @author Lakshman.Doke
 */
@Component
public class AbsDataServiceClient {
    private static final Logger logger = LoggerFactory.getLogger(AbsDataServiceClient.class);

    WebClient webClient = WebClient.builder().defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
            .build();
    @Autowired
    PropertyConfiguration propertyConfiguration;

    Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();

    public String callAbsDataService(String jsonToSend) {
        logger.info("callAbsDataService(String jsonToSend) started execution..........");
        List<BookingData> newBookingList = gson.fromJson(jsonToSend, new TypeToken<List<BookingData>>() {
        }.getType());
        ResponseEntity<?> response;
        try {
            response = webClient.post()
                    .uri(propertyConfiguration.getAbsDataServiceUrl())
                    .body(Flux.just(newBookingList), BookingData.class)
                    .retrieve().toEntity(String.class)
                    .block();
            if (response != null && response.getStatusCode() == HttpStatus.OK || response.getStatusCode() == HttpStatus.CREATED) {
                logger.info("callAbsDataService(String jsonToSend): Claim created !");
                return "Claim created";
            }
        } catch (Exception exception) {
            logger.error("callAbsDataService(String jsonToSend): Can't create claim: Exception {0}",exception);
        }
        return "";
    }
}
