package com.digit.utility;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

/**
 * 
 * @author Lakshman.Doke
 *
 */
@Component
@ConfigurationProperties(ignoreInvalidFields = true)
@EnableConfigurationProperties
@Data
public class PropertyConfiguration {
	@Value("${ABS_DATA_SERVICE_URL}")
	private String absDataServiceUrl;

	@Value("${GENERATE_APP_ID}")
	private String generateAppId;

	@Value("${API_KEY_DETAILS_URL}")
	private String apiKeyDetailsUrl;

	@Value("${API_KEY}")
	private String apiKey;

	@Value("${AUTH_URL}")
	private String authUrl;

	@Value("${PASSWORD}")
	private String password;

	@Value("${MAIL_SEND_URL}")
	private String mailSendUrl;

	@Value("${TNC_LINK_COMP}")
	private String tncLinkComp;

	@Value("${TNC_LINK_ARC}")
	private String tncLinkArc;

	@Value("${PARTNER}")
	private String partener;
	
	@Value("${EMAIL_AMMENDMENT_TEMPLATE}")
	private String emailAmmendmentTemplate;

	@Value("${PDF_GENERATE_URL}")
	private String pdfGenerateUrl;

	@Value("${CREATE_AMMENDMENT_POLICY_URL}")
	private String createAmendmentPolicyUrl;

	@Value("${TRAVEL_SERVICE_API_KEY}")
	private String travelServiceApiKey;

	@Value("${TRAVEL_DATA_SERVICE}")
	private String travelDataService;

	@Value("${TRAVEL_DATA_THRESHOLD}")
	private String travelDataThreshold;

	@Value("${GENERATE_PARTNER}")
	private String generatePartner;

	@Value("${SUMMARY_LINK_COMP}")
	private String summaryLinkComp;

	@Value("${SUMMARY_LINK_ARC}")
	private String summaryLinkArc;

	@Value("${SMS_SEND_URL}")
	private String smsSendUrl;

	@Value("${URL_SHORTNER_URL}")
	private String urlShortnerUrl;

	@Value("${SMS_TEMPLATE_NO}")
	private String smsTemplateNo;

	@Value("${SMS_AMMENDMENT_TEMPLATE_NO}")
	private String smsAmendmentTemplateNo;

	@Value("${CT_AMENDMENT_API_KEY}")
	private String ctAmendmentApiKey;

	@Value("${CREATE_POLICY_URL}")
	private String createPolicyUrl;
}
