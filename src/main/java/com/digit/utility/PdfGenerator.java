package com.digit.utility;

import org.springframework.beans.factory.annotation.Value;

import com.digit.model.Booking;
import com.digit.pdfgeneration.TravelPdfGeneration;
/**
 * 
 * @author Lakshman.Doke
 *
 */
public class PdfGenerator implements Runnable {
    @Value("${TRAVEL_SERVICE_API_KEY}")
    public static  String API_KEY;
    private final Booking booking;
    TravelPdfGeneration travelPdfGeneration = new TravelPdfGeneration();

    public PdfGenerator(Booking booking) {
        this.booking = booking;
    }

    @Override
    public void run() {
        travelPdfGeneration.generatePdf(booking);
    }
}
