package com.digit.utility;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import com.digit.model.UserCredentials;
import com.google.gson.GsonBuilder;

import reactor.core.publisher.Mono;

/**
 * 
 * @author Lakshman.Doke
 *
 */
@Component
public class AuthUtil {
	public static String authToken;
	private static final Logger logger = LoggerFactory.getLogger(AuthUtil.class);
	WebClient webClient = WebClient.builder().defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
			.build();
	@Autowired
	PropertyConfiguration propertyConfiguration;

	public boolean authenticate(String apiKey) {
		logger.info("authenticate(String apiKey): started execution..........");
		if ((apiKey == null || apiKey.isBlank())) {
			return false;
		}
		var user = new UserCredentials();
		user.setAppId(Long.parseLong(propertyConfiguration.getGenerateAppId()));
		user.setApiKey(apiKey);
		var objGson = new GsonBuilder().setPrettyPrinting().create();
		String credentials = objGson.toJson(user);

		ResponseEntity<?> response = webClient.post().uri(propertyConfiguration.getAuthUrl() + "3")
				.accept(MediaType.TEXT_HTML).body(Mono.just(credentials), String.class).retrieve()
				.toEntity(String.class).block();
		if (response != null && response.getStatusCode() == HttpStatus.OK
				|| response != null && response.getStatusCode() == HttpStatus.CREATED) {
			logger.info("authenticate(String apiKey): authentication is successful !");
			authToken = response.getHeaders().get(HttpHeaders.AUTHORIZATION).isEmpty() ? ""
					: response.getHeaders().get(HttpHeaders.AUTHORIZATION).get(0);
			return true;
		} else {
			logger.error("authenticate(String apiKey): authentication fail!");
			return false;
		}
	}
}
