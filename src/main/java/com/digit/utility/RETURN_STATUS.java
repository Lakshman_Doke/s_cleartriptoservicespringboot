package com.digit.utility;

import java.util.HashMap;
import java.util.Map;
/**
 * 
 * @author Lakshman.Doke
 *
 */
public enum RETURN_STATUS {

	PARTIAL("Claim triggered partially"), SUCCESS("Claim triggered successfully"),
	INELIGIBLE("Claim triggered failed. No eligible flights");

	static final Map<String, RETURN_STATUS> errorMap = new HashMap<>();

	private final String text;

	private RETURN_STATUS(final String text) {
		this.text = text;
	}

	public String toString() {
		return text;
	}

	public static RETURN_STATUS getErrorCode(String str) {
		return errorMap.get(str);
	}

	static {
		for (RETURN_STATUS c : RETURN_STATUS.values())
			errorMap.put(c.toString(), c);
	}
}
