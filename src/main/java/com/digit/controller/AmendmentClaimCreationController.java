package com.digit.controller;

import java.time.format.DateTimeParseException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.digit.entities.BookingData;
import com.digit.entities.Flight;
import com.digit.entities.Passenger;
import com.digit.exception.PolicyCreationException;
import com.digit.model.Booking;
import com.digit.repositories.BookingDataRepository;
import com.digit.utility.AuthUtil;
import com.digit.utility.ClaimValidationAndTrigger;
import com.digit.utility.Constants;
import com.digit.utility.RETURN_STATUS;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParseException;
/**
 * 
 * @author Lakshman.Doke
 *
 */
@RestController
@RequestMapping(value = "/rest")
public class AmendmentClaimCreationController {
	private static final Logger logger = LoggerFactory.getLogger(AmendmentClaimCreationController.class);

	@Autowired
	BookingDataRepository bookingDataRepository;

	@Autowired
	AuthUtil authUtil;

	@Autowired
	ClaimValidationAndTrigger claimValidationAndTrigger;

	Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().setPrettyPrinting().create();

	@PostMapping(value = "/createClaim", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> createClaim(@RequestHeader(HttpHeaders.AUTHORIZATION) String apiKey,
			@RequestBody String json) {
		logger.info("createClaim(@RequestHeader(HttpHeaders.AUTHORIZATION) String apiKey, String json): started .....");
		BookingData storedBooking = null;
		Booking booking = null;
		try {
			if (json == null || json.trim().isBlank()) {
				logger.info(
						"createClaim(@RequestHeader(HttpHeaders.AUTHORIZATION) String apiKey, String json): request body is null or empty .....");
				return new ResponseEntity<>("request body cannot be null / empty", HttpStatus.BAD_REQUEST);
			}
			if (!authUtil.authenticate(apiKey)) {
				logger.error(
						"createClaim(@RequestHeader(HttpHeaders.AUTHORIZATION) String apiKey, String json): Unauthorized : please check your api key.....");
				return new ResponseEntity<>("request body cannot be null / empty", HttpStatus.UNAUTHORIZED);
			}
			try {
				booking = gson.fromJson(json, Booking.class);
			} catch (JsonParseException e) {
				logger.info(
						"createClaim(@RequestHeader(HttpHeaders.AUTHORIZATION) String apiKey, String json): request body is null or empty .....");
				return new ResponseEntity<>("Bad request body", HttpStatus.BAD_REQUEST);
			}
			logger.info(
					"createClaim(@RequestHeader(HttpHeaders.AUTHORIZATION) String apiKey, String json): request received for trip id for claim: {}"
							,booking.getBooking().getTripId());
			logger.info(
					"createClaim(@RequestHeader(HttpHeaders.AUTHORIZATION) String apiKey, String json): createClaim : {}",
					booking);

			List<BookingData> bookingDataList = bookingDataRepository
					.getBookingDataByTripIdAndSubInsuranceProductCode(booking.getBooking().getTripId(), "C0");

			if (bookingDataList == null || bookingDataList.isEmpty()) {
				logger.error(
						"createClaim(@RequestHeader(HttpHeaders.AUTHORIZATION) String apiKey, String json): Booking details with booking id: {} "
								,booking.getBooking().getTripId() + "does not exists");
				return new ResponseEntity<>(
						"Booking details with booking id " + booking.getBooking().getTripId() + "does not exists",
						HttpStatus.BAD_REQUEST);
			}
			storedBooking = bookingDataList.get(0);
			Boolean isEligibleForClaim = claimValidationAndTrigger.compareNewAndStoredBooking(storedBooking,
					booking.getBooking());

			if (isEligibleForClaim != null && isEligibleForClaim) {
				booking.getBooking().setType(Constants.CLAIM_TYPE);
				claimValidationAndTrigger.triggerClaim(booking.getBooking());
			}
			int status = this.getStatus(booking);
			logger.info("createClaim(@RequestHeader(HttpHeaders.AUTHORIZATION) String apiKey, String json): return code: {} , for trip id {}"
					,status ,storedBooking.getTripId());
			storedBooking.setIsClaimTriggerSuccessful(true);
			bookingDataRepository.save(storedBooking);
			return new ResponseEntity<>(gson.toJson(booking), HttpStatus.valueOf(status));
		} catch (PolicyCreationException e) {
			logger.error(
					"createClaim(@RequestHeader(HttpHeaders.AUTHORIZATION) String apiKey, String json): Exception: ",
					e);
			this.updateBooking(booking, RETURN_STATUS.SUCCESS.toString());
			storedBooking.setIsClaimTriggerSuccessful(false);
			bookingDataRepository.save(storedBooking);

			return new ResponseEntity<>(booking, HttpStatus.valueOf(200));
		} catch (IllegalArgumentException | DateTimeParseException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.valueOf(400));
		} catch (Exception e) {
			logger.error(
					"createClaim(@RequestHeader(HttpHeaders.AUTHORIZATION) String apiKey, String json): Exception: ",
					e);
			return new ResponseEntity<>("Server error", HttpStatus.valueOf(500));
		}
	}

	@PostMapping(value = "/retryClaim", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> retryClaim(@RequestHeader(HttpHeaders.AUTHORIZATION) String apiKey, String days) {
		List<BookingData> bookingDataList = bookingDataRepository.findByisClaimTriggerSuccessful(false);
		bookingDataList.forEach(bookingData -> {
			try {
				bookingData.setType(Constants.CLAIM_TYPE);
				claimValidationAndTrigger.triggerClaim(bookingData);
				bookingDataRepository.save(bookingData);
			} catch (PolicyCreationException e) {
				logger.error(
						"retryClaim(@RequestHeader(HttpHeaders.AUTHORIZATION) String apiKey, String days): Exception: ",
						e);
			}
		});
		return null;
	}

	void updateBooking(Booking booking, String status) {
		var bookingData = booking.getBooking();
		bookingData.setUpdateStatus(status);
		booking.setBooking(bookingData);
	}

	public Integer getStatus(Booking booking) {

		List<Passenger> passengersList = booking.getBooking().getPassengers();
		var flightCount = 0;
		var successCount = 0;
		var failureCount = 0;

		for (Passenger passenger : passengersList) {
			List<Flight> flightList = passenger.getFlights();
			for (Flight flight : flightList) {
				if (flight.getUpdateStatus() != null && (flight.getUpdateStatus().equalsIgnoreCase(Constants.SUCCESS)
						|| flight.getUpdateStatus().equalsIgnoreCase(Constants.CLAIMED_FLIGHT))) {
					successCount++;
				} else {
					failureCount++;
				}
				flightCount++;
			}
		}

		if (flightCount == successCount)
			return 200;
		else if (successCount > 0 && failureCount > 0)
			return 207;
		else
			return 400;

	}
}
