package com.digit.controller;

import com.digit.entities.BookingData;
import com.digit.entities.Flight;
import com.digit.entities.Passenger;
import com.digit.model.Booking;
import com.digit.repositories.BookingDataRepository;
import com.digit.utility.Process;
import com.digit.utility.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.TimeZone;

/**
 * @author Lakshman.Doke
 */
@RestController
@RequestMapping("/rest")
public class BookingToDigitController {
    public static final int BAD_DATE_LENGTH = 16;
    private static final String colon = ":";
    private static final String bookingDateEnd = "00";
    private static final Logger logger = LoggerFactory.getLogger(BookingToDigitController.class);
    Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
    @Autowired
    AuthUtil authUtil ;
    @Autowired
    private BookingDataRepository bookingDataRepository;
    @Autowired
    private PropertyConfiguration propertyConfiguration;
    @Autowired
    private Process process;

    @PostMapping(value = "/issuePolicy", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> send(@RequestHeader(HttpHeaders.AUTHORIZATION) String apiKey, @RequestBody String json) {
        if (null==json || json.trim().isEmpty())
            return new ResponseEntity<>("request body cannot be null", HttpStatus.BAD_REQUEST);
        try {
        	  if (!authUtil.authenticate(apiKey))
                  return new ResponseEntity<>("Unauthorized : please check your api key", HttpStatus.UNAUTHORIZED);
        }catch (Exception e) {
        	  return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
      
        var ret = "";
        var booking = new Booking();
        try {
            booking = gson.fromJson(json, Booking.class);
            booking = JsonFilter.filter(booking);
        } catch (JsonParseException e) {
            logger.error("send(@RequestHeader(HttpHeaders.AUTHORIZATION) String apiKey, String json): Exception: ", e);
            return new ResponseEntity<>("Bad request body", HttpStatus.BAD_REQUEST);
        }
        try {
            logger.info(
                    "send(@RequestHeader(HttpHeaders.AUTHORIZATION) String apiKey, String json): request received for trip id : {}"
                    , booking.getBooking().getTripId());
            List<BookingData> bookingDataList = bookingDataRepository.getBookingDataByTripIdAndSubInsuranceProductCode(
                    booking.getBooking().getTripId(), booking.getBooking().getSubInsuranceProductCode());
            if (null == bookingDataList || bookingDataList.isEmpty()) {
                var bookingData = booking.getBooking();
                bookingData.setCreatedDate(new java.sql.Timestamp(System.currentTimeMillis()));
                bookingData.setLastModifiedDate(new java.sql.Timestamp(System.currentTimeMillis()));
                bookingDataRepository.save(bookingData);
            } else {
                booking.setBooking(bookingDataList.get(0));
            }
            List<Passenger> passengersList = booking.getBooking().getPassengers();
            for (Passenger passenger : passengersList) {
                if (passenger.getFlights() == null || passenger.getFlights().isEmpty()) {
                    return new ResponseEntity<>("Flights cannot be null or empty", HttpStatus.BAD_REQUEST);
                }
            }
            var newBooking = process.process(booking, apiKey);
            try {
                bookingDataRepository.save(newBooking.getBooking());
            } catch (Exception e) {
                logger.error("send(@RequestHeader(HttpHeaders.AUTHORIZATION) String apiKey, String json): Exception:", e);
            }
            ret = gson.toJson(newBooking);
            return new ResponseEntity<>(ret, HttpStatus.OK);
        } catch (Exception e) {
            logger.error("send(@RequestHeader(HttpHeaders.AUTHORIZATION) String apiKey, String json): Exception:", e);
            return new ResponseEntity<>("Server Error", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/retry")
    public ResponseEntity<?> retry(@RequestParam(value = "days") String days) {
        String apiKey = null;
        try {
            var dateTimeFormatter = DateTimeFormatter.ofPattern(Constants.DATE_FORMAT);
            var datetime1 = LocalDateTime.now(ZoneId.of("GMT+05:30"));
            var formatDateTime = datetime1.format(dateTimeFormatter);
            List<Long> missedBookings = bookingDataRepository.getMissedBookingsId();
            for (Long missedBooking : missedBookings) {
                Optional<BookingData> bookingDataOptional = bookingDataRepository.findById(missedBooking);
                var bookingData = bookingDataOptional.orElseGet(BookingData::new);
                logger.error("retry(@RequestParam(value = \"days\"): working on booking id: {}", bookingData.getTripId());
                List<Long> missedBookingsReCheck = bookingDataRepository.getMissedBookingsId();
                Long currentBookingId = bookingData.getBookingDataId();
                Timestamp lastModifiedDate = bookingData.getLastModifiedDate();
                var lastModifiedDateTime = lastModifiedDate.toLocalDateTime();
                var currentDateTimeToCompare = LocalDateTime.now();
                if (!missedBookingsReCheck.contains(currentBookingId) || bookingData.isInRetry()
                        || (lastModifiedDateTime.until(currentDateTimeToCompare, ChronoUnit.MINUTES) < 15))
                    continue;
                var bookingDateString = bookingData.getBookingDate();
                if (bookingDateString.length() == BAD_DATE_LENGTH) {
                    bookingData.setBookingDate(bookingDateString + colon + bookingDateEnd);
                }
                var bookingDateTime = LocalDateTime.parse(bookingDateString, dateTimeFormatter);
                var currentDateTime = LocalDateTime.parse(formatDateTime, dateTimeFormatter);
                var currentDate = currentDateTime.toLocalDate();
                var bookingDate = bookingDateTime.toLocalDate();
                if (currentDate.minusDays(Integer.parseInt(days)).isBefore(bookingDate)
                        || currentDate.minusDays(Integer.parseInt(days)).isEqual(bookingDate)
                        || currentDate.equals(bookingDate)) {
                    if (!bookingData.getCustomer().getPinCode().matches("[0-9]+")) {
                        bookingData.getCustomer().setPinCode("");
                    }
                    bookingData.setInRetry(true);
                    var booking = new Booking();
                    booking.setBooking(bookingData);
                    String json = gson.toJson(booking);

                    if ("C0".equals(booking.getBooking().getSubInsuranceProductCode())) {
                        apiKey = propertyConfiguration.getCtAmendmentApiKey();
                    } else {
                        apiKey = propertyConfiguration.getApiKey();
                    }

                    send(apiKey, json);
                    bookingData = bookingDataOptional.orElseGet(BookingData::new);
                    var failureFlag = 0;
                    for (Passenger passenger : bookingData.getPassengers()) {
                        if (passenger.getPolicyNumber() == null || passenger.getPolicyNumber().equals("NA")) {
                            failureFlag = 1;
                            break;
                        }
                    }
                    bookingData.setInRetry(failureFlag != 1);
                    bookingDataRepository.save(bookingData);
                }
                logger.info("retry(@RequestParam(value = \"days\"): done for booking id  {}", bookingData.getTripId());
            }
            return new ResponseEntity<>("Done", HttpStatus.OK);
        } catch (Exception exception) {
            logger.error("retry(@RequestParam(value = \"days\"): Exception:  {0}", exception);
            return new ResponseEntity<>("Failed", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public void checkBookingDate(Booking booking) {
        var bookingDateString = booking.getBooking().getBookingDate();
        boolean isLengthSixteen = bookingDateString.length() == 16;
        if (isLengthSixteen) {
            bookingDateString += Constants.EXTRA_SECONDS;
            booking.getBooking().setBookingDate(bookingDateString);
        }
        var sdf2 = new SimpleDateFormat(Constants.DATE_FORMAT, Locale.ENGLISH);
        sdf2.setTimeZone(TimeZone.getTimeZone("IST"));
        booking = getBookingWIthSetValue(booking);
    }

    public boolean isJourneyTypeOnward(Flight flight) {
        return (flight.getJourneyType().equalsIgnoreCase("Onward")
                || (flight.getJourneyType().equalsIgnoreCase("Onwards")))
                && flight.getLeg().equalsIgnoreCase("1");
    }
    public Booking getBookingWIthSetValue(Booking booking){
        var dateTimeFormatter = DateTimeFormatter.ofPattern(Constants.DATE_FORMAT);
        var bookingDateTime = LocalDateTime.parse(booking.getBooking().getBookingDate(), dateTimeFormatter);
        var datetime1 = LocalDateTime.now(ZoneId.of("GMT+05:30"));
        String formatDateTime = datetime1.format(dateTimeFormatter);
        var currentDateTime = LocalDateTime.parse(formatDateTime, dateTimeFormatter);
        if (bookingDateTime.isBefore(currentDateTime)) {
            long diffHours = bookingDateTime.until(currentDateTime, ChronoUnit.HOURS);
            if (diffHours < 48) {
                LocalDateTime departureDate = null;
                List<Flight> flightList = booking.getBooking().getPassengers().get(0).getFlights();
                for (Flight flight : flightList) {
                    if (isJourneyTypeOnward(flight)) {
                        var depString = flight.getDepartureDate();
                        if (depString.length() == 20)
                            departureDate = LocalDateTime.parse(depString.substring(0, depString.length() - 1),
                                    dateTimeFormatter);
                        else if (depString.length() == 19)
                            departureDate = LocalDateTime.parse(depString, dateTimeFormatter);
                    }
                }
                if (departureDate != null && departureDate.isAfter(currentDateTime.plusMinutes(30)))
                    booking.getBooking().setBookingDate(String.valueOf(currentDateTime));
            }
        }
        return booking;
    }
}
