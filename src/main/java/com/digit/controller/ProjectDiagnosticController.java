package com.digit.controller;

import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;

import com.digit.diagnostic.ABSDiagnostic;
import com.digit.diagnostic.DBConnectvityDiagnostic;
import com.digit.diagnostic.DiagnosticResult;
import com.digit.utility.PropertyConfiguration;
import com.google.gson.GsonBuilder;

import reactor.core.publisher.Mono;

/**
 * @author Lakshman.Doke
 */
@RequestMapping("/rest")
@RestController
public class ProjectDiagnosticController {
    private static final Logger logger = LoggerFactory.getLogger(ProjectDiagnosticController.class);
    @Autowired
    SessionFactory sessionFactory;
    @Autowired
    private PropertyConfiguration propertyConfiguration;

    @GetMapping("/diagnostic")
    public ResponseEntity<String> testService() {
        try {
            ResponseEntity<Object> dispatcherResponse = WebClient.create(propertyConfiguration.getCreatePolicyUrl())
                    .post()
                    .accept(MediaType.APPLICATION_JSON)
                    .body(Mono.empty(),Object.class)
                    .retrieve()
                    .onStatus(HttpStatus::isError,
							response -> response.bodyToMono(String.class)
									.flatMap(error -> Mono.error(new RuntimeException(error))))
                    .toEntity(Object.class)
                    .block();
            var result = new DiagnosticResult();
            var abs = new ABSDiagnostic();
            var db = new DBConnectvityDiagnostic();
            result.setFile_temp_location("NA");
            if (sessionFactory.isOpen()) {
                db.setDBConnectivity("Available");
                db.setStatus("Success");
            } else {
                db.setDBConnectivity("Not Available");
                db.setStatus("Failed");
            }
            if (dispatcherResponse == null) throw new AssertionError();
            if (dispatcherResponse.getStatusCode() != HttpStatus.BAD_GATEWAY || dispatcherResponse.getStatusCode() != HttpStatus.SERVICE_UNAVAILABLE
                    || dispatcherResponse.getStatusCode() != HttpStatus.GATEWAY_TIMEOUT) {
                abs.setMessage("Dispatcher Working");
                abs.setStatus("Success");
            } else {
                abs.setMessage("Dispatcher Not Working");
                abs.setStatus("Failed");
            }
            result.setAbsDiagnostic(abs);
            result.setDataBaseConnectivity(db);
            var gson = new GsonBuilder().setPrettyPrinting().create();
            String response = gson.toJson(result);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception exception) {
            logger.error("testService(): Exception: ", exception);
            return new ResponseEntity<>("Server Error", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
