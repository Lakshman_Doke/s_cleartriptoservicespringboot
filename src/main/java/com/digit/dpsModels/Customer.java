package com.digit.dpsModels;
/**
 * 
 */
import java.util.List;

import com.digit.model.PolicyStatus;

import lombok.Data;
@Data
public class Customer {
    private String title;

    private String firstName;

    private String lastName;

    private Address address;

    private String dob;

    private String mobile;

    private String email;

    private String profession;

    private String paxId;

    private String policyID;

    private String transactionDate;

    private PolicyStatus status;

    private String policyStatus;

    private Nominee nominee;

    private String type;

    private String gender;

    private String policyNumber;

    private String uid;

    private String skuId;

    private String applicationId;

    private List<FlightDetails> flights;

    private String errorMessage;

    private Boolean paymentLinkSent;

    private String companyName;

    private String paxIdentifier;

    private List<Document> documents;
}
