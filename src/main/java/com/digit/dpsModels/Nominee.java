package com.digit.dpsModels;

import lombok.Data;
/**
 * 
 * @author Lakshman.Doke
 *
 */
@Data
public class Nominee {
    private String relationship;

    private String dob;

    private String name;
}
