package com.digit.dpsModels;

import java.util.List;

import com.digit.entities.Payment;
import com.digit.model.Coverage;
import com.digit.model.PolicyStatus;
/**
 * 
 * @author Lakshman.Doke
 *
 */
public class Policy {
	private String id;

	private String bookingId;

	private String type;

	private String purpose;

	private Customer customer;

	private Payment payment;

	private List<Customer> insuredPersons;

	private String insuredPersonsCount;

	private String bookingDate;

	private String packageName;

	private String geography;

	private String travelEndDate;

	private String travelStartDate;

	private String countryTravel;

	private PolicyStatus status;

	private Customer insuredPerson;

	private List<Coverage> domesticCoverages;

	private String quoteId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getBookingId() {
		return bookingId;
	}

	public void setBookingId(String bookingId) {
		this.bookingId = bookingId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPurpose() {
		return purpose;
	}

	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Payment getPayment() {
		return payment;
	}

	public void setPayment(Payment payment) {
		this.payment = payment;
	}

	public List<Customer> getInsuredPersons() {
		return insuredPersons;
	}

	public void setInsuredPersons(List<Customer> insuredPersons) {
		this.insuredPersons = insuredPersons;
	}

	public String getInsuredPersonsCount() {
		return insuredPersonsCount;
	}

	public void setInsuredPersonsCount(String insuredPersonsCount) {
		this.insuredPersonsCount = insuredPersonsCount;
	}

	public String getBookingDate() {
		return bookingDate;
	}

	public void setBookingDate(String bookingDate) {
		this.bookingDate = bookingDate;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public String getGeography() {
		return geography;
	}

	public void setGeography(String geography) {
		this.geography = geography;
	}

	public String getTravelEndDate() {
		return travelEndDate;
	}

	public void setTravelEndDate(String travelEndDate) {
		this.travelEndDate = travelEndDate;
	}

	public String getTravelStartDate() {
		return travelStartDate;
	}

	public void setTravelStartDate(String travelStartDate) {
		this.travelStartDate = travelStartDate;
	}

	public String getCountryTravel() {
		return countryTravel;
	}

	public void setCountryTravel(String countryTravel) {
		this.countryTravel = countryTravel;
	}

	public PolicyStatus getStatus() {
		return status;
	}

	public void setStatus(PolicyStatus status) {
		this.status = status;
	}

	public Customer getInsuredPerson() {
		return insuredPerson;
	}

	public void setInsuredPerson(Customer insuredPerson) {
		this.insuredPerson = insuredPerson;
	}

	public List<Coverage> getDomesticCoverages() {
		return domesticCoverages;
	}

	public void setDomesticCoverages(List<Coverage> domesticCoverages) {
		this.domesticCoverages = domesticCoverages;
	}

	public String getQuoteId() {
		return quoteId;
	}

	public void setQuoteId(String quoteId) {
		this.quoteId = quoteId;
	}
}
