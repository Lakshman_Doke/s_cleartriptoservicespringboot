package com.digit.dpsModels;

import lombok.Data;
/**
 * 
 * @author Lakshman.Doke
 *
 */
@Data
public class Document {
    private String type;

    private String id;

    private String expiryDate;

    private String issuingAuthority;

    private String issuingPlace;
}
