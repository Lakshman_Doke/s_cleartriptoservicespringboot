package com.digit.dpsModels;

import lombok.Data;
/**
 * 
 * @author Lakshman.Doke
 *
 */
@Data
public class FlightDetails {
    private String updateType;
    private String journeyType;
    private String leg;
    private String source;
    private String destination;
    private String startDate;
    private String endDate;
    private String providerName;
    private String carrierNo;
    private String providerTravelNo;
    private String classType;
    private String ticketNumber;
    private String refundableFlag;
    private String originalCarrierName;
    private String originalCarrierNo;
    private String originalCarrierFlightNo;
    private String originalAirlinePNR;
    private String packageName;
    private String travelMode;
    private String providerTravelNumber;
    private String price;
    private String baseFare;
    private String feesSurcharge;
    private String convenienceFee;
    private String cancellationCharge;
    private String fareType;
    private String ticketPrice;
}
